module.exports =
    {
        extends: ['@commitlint/config-conventional'],
        rules: {
            "footer-leading-blank": [0],
            "header-max-length": [2, "always", 120],
            "body-max-line-length": [2, "always", 150]
        }
    }
