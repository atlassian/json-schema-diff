/* eslint-disable max-classes-per-file */

import {JsonSchema, SimpleTypes} from 'json-schema-spec-types';
import {addAnyOfIfNecessary, Set, Subset} from './set';
import {ParseContext} from '../../config';

interface TypeSet<T extends SimpleTypes> extends Set<T> {
    intersectWithSome(other: SomeTypeSet<T>): Set<T>;

    unionWithSome(other: SomeTypeSet<T>): Set<T>;
}

class AllTypeSet<T extends SimpleTypes> implements TypeSet<T> {
    public readonly type = 'all';

    public constructor(public readonly setType: T) {
    }

    public intersect(other: TypeSet<T>): TypeSet<T> {
        return other;
    }

    public intersectWithSome(other: SomeTypeSet<T>): Set<T> {
        return other;
    }

    public union(): TypeSet<T> {
        return this;
    }

    public unionWithSome(): Set<T> {
        return this;
    }

    public complement(): TypeSet<T> {
        return new EmptyTypeSet(this.setType);
    }

    public isSubsetOf(): boolean {
        throw new Error('Not Implemented');
    }

    public toJsonSchema(): JsonSchema {
        return {type: this.setType};
    }
}

class EmptyTypeSet<T extends SimpleTypes> implements TypeSet<T> {
    public readonly type = 'empty';

    public constructor(public readonly setType: T) {
    }

    public intersect(): TypeSet<T> {
        return this;
    }

    public intersectWithSome(): Set<T> {
        return this;
    }

    public union(other: TypeSet<T>): TypeSet<T> {
        return other;
    }

    public unionWithSome(other: SomeTypeSet<T>): Set<T> {
        return other;
    }

    public complement(): TypeSet<T> {
        return new AllTypeSet(this.setType);
    }

    public isSubsetOf(): boolean {
        return true;
    }

    public toJsonSchema(): JsonSchema {
        return false;
    }
}

class SomeTypeSet<T extends SimpleTypes> implements TypeSet<T> {
    public readonly type = 'some';

    public constructor(public readonly setType: T, private readonly subsets: Array<Subset<T>>, private readonly context: ParseContext) {
    }

    public complement(): Set<T> {
        const complementedSubsets = this.subsets
            .map((set) => set.complement())
            .reduce((subsetListA, subsetListB) => this.intersectSubsets(subsetListA, subsetListB));
        return createTypeSetFromSubsets(this.setType, complementedSubsets, this.context);
    }

    public intersect(other: TypeSet<T>): Set<T> {
        return other.intersectWithSome(this);
    }

    public intersectWithSome(other: SomeTypeSet<T>): Set<T> {
        const intersectedSubsets = this.intersectSubsets(this.subsets, other.subsets);
        return createTypeSetFromSubsets(this.setType, intersectedSubsets, this.context);
    }

    public union(other: TypeSet<T>): Set<T> {
        return other.unionWithSome(this);
    }

    public unionWithSome(other: SomeTypeSet<T>): Set<T> {
        const newSubsets = [...this.subsets, ...other.subsets];
        return createTypeSetFromSubsets(this.setType, newSubsets, this.context);
    }

    public isSubsetOf(other: TypeSet<T>): boolean {
        return other.complement().intersect(this).type === 'empty';
    }

    public toJsonSchema(): JsonSchema {
        return addAnyOfIfNecessary(this.subsets.map((subset) => subset.toJsonSchema()));
    }

    private intersectSubsets<U extends SimpleTypes>(
        subsetListA: Array<Subset<U>>,
        subsetListB: Array<Subset<U>>
    ): Array<Subset<U>> {
        let intersectedSubsets: Array<{ subset: Subset<U>, typeSet: TypeSet<U> }> = [];

        for (const subsetA of subsetListA) {
            for (const subsetB of subsetListB) {
                const intersectionSubset = subsetA.intersect(subsetB);
                const intersectionTypeSet = createTypeSetFromSubsets(intersectionSubset.setType, [intersectionSubset], this.context);

                if (this.shouldOmitIntersectionBasedOnHeuristics(intersectedSubsets, intersectionTypeSet)) {
                    continue;
                }

                intersectedSubsets = this.simplifyIntersectedSubsetsWithHeuristics(intersectedSubsets, intersectionTypeSet);
                intersectedSubsets.push({subset: intersectionSubset, typeSet: intersectionTypeSet});
            }
        }

        return intersectedSubsets.map((s) => s.subset);
    }

    private shouldOmitIntersectionBasedOnHeuristics<U extends SimpleTypes>(
        intersectedSubsets: Array<{ subset: Subset<U>, typeSet: TypeSet<U> }>,
        intersectionTypeSet: TypeSet<U>
    ): boolean {
        if (this.context.heuristics.applyLawOfAbsorptionInTypeSetCartesianProduct) {
            const isIntersectionAlreadyRepresented =
                intersectedSubsets.some((s) => intersectionTypeSet.isSubsetOf(s.typeSet));
            return isIntersectionAlreadyRepresented;
        }

        return false;
    }

    private simplifyIntersectedSubsetsWithHeuristics<U extends SimpleTypes>(
        intersectedSubsets: Array<{ subset: Subset<U>, typeSet: TypeSet<U> }>,
        intersectionTypeSet: TypeSet<U>
    ): Array<{ subset: Subset<U>, typeSet: TypeSet<U> }> {
        return this.context.heuristics.applyLawOfAbsorptionInTypeSetCartesianProduct
            ? intersectedSubsets.filter((s) => !s.typeSet.isSubsetOf(intersectionTypeSet))
            : intersectedSubsets;
    }
}

const isAnySubsetTypeAll = <T>(subsets: Array<Subset<T>>): boolean => subsets.some((subset) => subset.type === 'all');

const isTypeAll = <T>(subsets: Array<Subset<T>>): boolean => {
    return isAnySubsetTypeAll(subsets);
};

const getNonEmptySubsets = <T>(subsets: Array<Subset<T>>): Array<Subset<T>> =>
    subsets.filter((subset) => subset.type !== 'empty');

export const createTypeSetFromSubsets = <T extends SimpleTypes>(
    setType: T,
    subsets: Array<Subset<T>>,
    context: ParseContext
): TypeSet<T> => {
    const notEmptySubsets = getNonEmptySubsets(subsets);

    if (notEmptySubsets.length === 0) {
        return new EmptyTypeSet(setType);
    }

    if (isTypeAll(subsets)) {
        return new AllTypeSet(setType);
    }

    return new SomeTypeSet(setType, notEmptySubsets, context);
};
