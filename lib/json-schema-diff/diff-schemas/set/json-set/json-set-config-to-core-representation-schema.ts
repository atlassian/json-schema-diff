import {CoreSchemaMetaSchema, JsonSchema, SimpleTypes} from 'json-schema-spec-types';
import {addAnyOfIfNecessary} from '../set';
import {JsonSetConfig} from './json-set-config';

const createEmptyCoreSchemaMetaSchema = (): CoreSchemaMetaSchema => ({type: []});

const getTypeOrEmpty = (jsonSchema: CoreSchemaMetaSchema): SimpleTypes[] => {
    if (!jsonSchema.type) {
        return [];
    }

    if (typeof jsonSchema.type === 'string') {
        return [jsonSchema.type];
    }

    return jsonSchema.type;
};

const getAnyOfOrEmpty = (jsonSchema: CoreSchemaMetaSchema): JsonSchema[] =>
    jsonSchema.anyOf || [];

const isSimpleSchema = (schema: CoreSchemaMetaSchema): boolean => schema.anyOf === undefined;

const mergeTypes = (schema: CoreSchemaMetaSchema, otherSchema: CoreSchemaMetaSchema): SimpleTypes | SimpleTypes[] => {
    const schemaTypes = getTypeOrEmpty(schema);
    const otherSchemaTypes = getTypeOrEmpty(otherSchema);
    const mergedTypes = schemaTypes.concat(otherSchemaTypes);

    return (mergedTypes.length === 1) ? mergedTypes[0] : mergedTypes;
};

const mergeCoreRepresentationJsonSchemas = (
    schema: CoreSchemaMetaSchema,
    otherSchema: CoreSchemaMetaSchema
): CoreSchemaMetaSchema => {
    const mergedSchema = {
        ...schema,
        ...otherSchema,
        type: mergeTypes(schema, otherSchema)
    };

    if (schema.not && otherSchema.not) {
        mergedSchema.not = mergeCoreRepresentationJsonSchemas(
            toCoreRepresentationJsonSchema(schema.not),
            toCoreRepresentationJsonSchema(otherSchema.not)
        );
    }

    return mergedSchema;
};

const isCoreSchemaMetaSchema = (schema: JsonSchema): schema is CoreSchemaMetaSchema => typeof schema !== 'boolean';

const toCoreRepresentationJsonSchema = (schema: JsonSchema): CoreSchemaMetaSchema =>
    isCoreSchemaMetaSchema(schema)
        ? schema
        : createEmptyCoreSchemaMetaSchema();

export const jsonSetConfigToCoreRepresentationSchema = (config: JsonSetConfig): JsonSchema => {
    const typeSetSchemas = Object
        .keys(config)
        .map((typeSetName: keyof JsonSetConfig) => config[typeSetName].toJsonSchema())
        .filter(isCoreSchemaMetaSchema);

    const schemas = typeSetSchemas
        .filter((schema) => !isSimpleSchema(schema))
        .reduce((accumulator, schema) => {
            const anyOf = getAnyOfOrEmpty(schema);
            return accumulator.concat(anyOf);
        }, []);

    const mergedSimpleSubsetSchemas = typeSetSchemas
        .filter(isSimpleSchema)
        .reduce((mergedSchema, schema) => {
            return mergeCoreRepresentationJsonSchemas(mergedSchema, schema);
        }, createEmptyCoreSchemaMetaSchema());

    if (getTypeOrEmpty(mergedSimpleSubsetSchemas).length > 0) {
        schemas.push(mergedSimpleSubsetSchemas);
    }

    return addAnyOfIfNecessary(schemas);
};
