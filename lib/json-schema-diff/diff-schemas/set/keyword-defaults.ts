import {SimpleTypes} from 'json-schema-spec-types';
import {ParsedPropertiesKeyword} from './subset/object-subset/object-subset-config';

export const defaultProperties: ParsedPropertiesKeyword = {};
export const defaultMaxItems: number = Number.POSITIVE_INFINITY;
export const defaultMaxProperties: number = Number.POSITIVE_INFINITY;
export const defaultMinItems: number = 0;
export const defaultMinProperties: number = 0;
export const defaultRequired: string[] = [];
export const defaultMaxLength: number = Number.POSITIVE_INFINITY;
export const defaultMinLength: number = 0;
export const defaultMinimum: number = Number.NEGATIVE_INFINITY;
export const defaultMaximum: number = Number.POSITIVE_INFINITY;
export const defaultTypes: SimpleTypes[] = ['array', 'boolean', 'integer', 'null', 'number', 'object', 'string'];
