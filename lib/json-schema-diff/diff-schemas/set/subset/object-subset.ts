import {JsonSchema} from 'json-schema-spec-types';
import {Subset} from '../set';
import {complementObjectSubsetConfig} from './object-subset/complement-object-subset-config';
import {intersectObjectSubsetConfig} from './object-subset/intersect-object-subset-config';
import {ObjectSubsetConfig} from './object-subset/object-subset-config';
import {objectSubsetConfigHasContradictions} from './object-subset/object-subset-config-has-contradictions';
import {objectSubsetConfigToJsonSchema} from './object-subset/object-subset-config-to-json-schema';
import {simplifyObjectSubsetConfig} from './object-subset/simplify-object-subset-config';
import {AllSubset, EmptySubset} from './subset';

class SomeObjectSubset implements Subset<'object'> {
    public readonly type = 'some';
    public readonly setType = 'object';

    public constructor(private readonly config: ObjectSubsetConfig) {
    }

    public get properties() {
        return this.config.properties;
    }

    public complement(): Array<Subset<'object'>> {
        return complementObjectSubsetConfig(this.config).map(createObjectSubsetFromConfig);
    }

    public intersect(other: Subset<'object'>): Subset<'object'> {
        return other.intersectWithSome(this);
    }

    public intersectWithSome(other: SomeObjectSubset): Subset<'object'> {
        return createObjectSubsetFromConfig(intersectObjectSubsetConfig(this.config, other.config));
    }

    public toJsonSchema(): JsonSchema {
        return objectSubsetConfigToJsonSchema(this.config);
    }
}

export const allObjectSubset = new AllSubset('object');
export const emptyObjectSubset = new EmptySubset('object');
export const createObjectSubsetFromConfig = (config: ObjectSubsetConfig): Subset<'object'> => {
    const simplifiedConfig = simplifyObjectSubsetConfig(config);

    return objectSubsetConfigHasContradictions(simplifiedConfig)
        ? emptyObjectSubset
        : new SomeObjectSubset(simplifiedConfig);
};
