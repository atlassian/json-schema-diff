import {defaultMaximum, defaultMinimum} from '../../keyword-defaults';
import {NumberSubsetConfig} from './number-subset-config';

const getUnrestrictedNumberSubsetConfig = (): NumberSubsetConfig => ({
    exclusiveMaximum: defaultMaximum,
    exclusiveMinimum: defaultMinimum,
    maximum: defaultMaximum,
    minimum: defaultMinimum,
    isInteger: false,
    not: []
});

const complementNot = (config: NumberSubsetConfig): NumberSubsetConfig[] =>
    config.not;

const complementMaximum = (config: NumberSubsetConfig): NumberSubsetConfig => ({
    ...getUnrestrictedNumberSubsetConfig(),
    exclusiveMinimum: config.maximum
});

const complementMinimum = (config: NumberSubsetConfig): NumberSubsetConfig => ({
    ...getUnrestrictedNumberSubsetConfig(),
    exclusiveMaximum: config.minimum
});

const complementExclusiveMaximum = (config: NumberSubsetConfig): NumberSubsetConfig => ({
    ...getUnrestrictedNumberSubsetConfig(),
    minimum: config.exclusiveMaximum
});

const complementExclusiveMinimum = (config: NumberSubsetConfig): NumberSubsetConfig => ({
    ...getUnrestrictedNumberSubsetConfig(),
    maximum: config.exclusiveMinimum
});

const complementIsInteger = (config: NumberSubsetConfig): NumberSubsetConfig => ({
    ...getUnrestrictedNumberSubsetConfig(),
    not: [{
        ...getUnrestrictedNumberSubsetConfig(),
        isInteger: config.isInteger
    }]
});

export const complementNumberSubsetConfig = (config: NumberSubsetConfig): NumberSubsetConfig[] =>
    [
        complementExclusiveMaximum(config),
        complementExclusiveMinimum(config),
        complementMaximum(config),
        complementMinimum(config),
        complementIsInteger(config),
        ...complementNot(config)
    ];
