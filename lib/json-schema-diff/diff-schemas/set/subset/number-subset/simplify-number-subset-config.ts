import {defaultMaximum, defaultMinimum} from '../../keyword-defaults';
import {NumberSubsetConfig} from './number-subset-config';

const normaliseRangesForIntegers = (config: NumberSubsetConfig): NumberSubsetConfig =>
    config.isInteger
        ? {
            ...config,
            minimum: Math.ceil(config.minimum),
            maximum: Math.floor(config.maximum),
            exclusiveMinimum: Math.floor(config.exclusiveMinimum),
            exclusiveMaximum: Math.ceil(config.exclusiveMaximum)
        }
        : {...config};

const mostRestrictiveMinimum = (config: NumberSubsetConfig): Pick<NumberSubsetConfig, 'minimum' | 'exclusiveMinimum'> =>
    config.minimum > config.exclusiveMinimum
        ? {minimum: config.minimum, exclusiveMinimum: defaultMinimum}
        : {minimum: defaultMinimum, exclusiveMinimum: config.exclusiveMinimum};

const mostRestrictiveMaximum = (config: NumberSubsetConfig): Pick<NumberSubsetConfig, 'maximum' | 'exclusiveMaximum'> =>
    config.maximum < config.exclusiveMaximum
        ? {maximum: config.maximum, exclusiveMaximum: defaultMaximum}
        : {maximum: defaultMaximum, exclusiveMaximum: config.exclusiveMaximum};

export const simplifyNumberSubsetConfig = (config: NumberSubsetConfig): NumberSubsetConfig => {
    config = normaliseRangesForIntegers(config);
    return {
        ...config,
        ...mostRestrictiveMinimum(config),
        ...mostRestrictiveMaximum(config)
    };
};
