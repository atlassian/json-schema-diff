import {NumberSubsetConfig} from './number-subset-config';

const isIntegerAndNotIsIntegerContradiction = (config: NumberSubsetConfig): boolean =>
    config.not.some((subset) => subset.isInteger === config.isInteger);

const isMinimumAndMaximumContradiction = (config: NumberSubsetConfig): boolean =>
    config.minimum >= config.exclusiveMaximum
    || config.minimum > config.maximum
    || config.exclusiveMinimum >= Math.min(config.maximum, config.exclusiveMaximum);

const isIntegerNotInExclusiveRange = (config: NumberSubsetConfig): boolean =>
    config.isInteger && config.exclusiveMaximum - config.exclusiveMinimum === 1;

type contradictionTestFn = (config: NumberSubsetConfig) => boolean;

const contradictionTests: contradictionTestFn[] = [
    isIntegerAndNotIsIntegerContradiction,
    isMinimumAndMaximumContradiction,
    isIntegerNotInExclusiveRange
];

export const numberSubsetConfigHasContradictions = (config: NumberSubsetConfig): boolean =>
    contradictionTests.some((contradictionTest) => contradictionTest(config));
