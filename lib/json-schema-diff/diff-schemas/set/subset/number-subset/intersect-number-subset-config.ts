import {NumberSubsetConfig} from './number-subset-config';

export const intersectNumberSubsetConfig = (
    configA: NumberSubsetConfig,
    configB: NumberSubsetConfig
): NumberSubsetConfig => ({
    exclusiveMaximum: Math.min(configA.exclusiveMaximum, configB.exclusiveMaximum),
    exclusiveMinimum: Math.max(configA.exclusiveMinimum, configB.exclusiveMinimum),
    maximum: Math.min(configA.maximum, configB.maximum),
    minimum: Math.max(configA.minimum, configB.minimum),
    isInteger: configA.isInteger || configB.isInteger,
    not:  [...configA.not, ...configB.not]
});
