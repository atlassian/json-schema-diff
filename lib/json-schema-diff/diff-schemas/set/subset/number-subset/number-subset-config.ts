export interface NumberSubsetConfig {
    minimum: number;
    maximum: number;
    exclusiveMinimum: number;
    exclusiveMaximum: number;
    isInteger: boolean;
    not: NumberSubsetConfig[];
}
