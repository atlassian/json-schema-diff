import {CoreSchemaMetaSchema, JsonSchema} from 'json-schema-spec-types';
import {defaultMaximum, defaultMinimum} from '../../keyword-defaults';
import {NumberSubsetConfig} from './number-subset-config';

const getNotSchema = (config: NumberSubsetConfig): CoreSchemaMetaSchema =>
    // TODO: write test to force an anyOf, is it possible?
    config.not.length === 0 ? {} : {not: numberSubsetConfigToJsonSchema(config.not[0])};

const getExclusiveMaximum = (config: NumberSubsetConfig): CoreSchemaMetaSchema =>
    config.exclusiveMaximum === defaultMaximum ? {} : {exclusiveMaximum:  config.exclusiveMaximum};

const getExclusiveMinimum = (config: NumberSubsetConfig): CoreSchemaMetaSchema =>
    config.exclusiveMinimum === defaultMinimum ? {} : {exclusiveMinimum:  config.exclusiveMinimum};

const getMaximum = (config: NumberSubsetConfig): CoreSchemaMetaSchema =>
    config.maximum === defaultMaximum ? {} : {maximum: config.maximum};

const getMinimum = (config: NumberSubsetConfig): CoreSchemaMetaSchema =>
    config.minimum === defaultMinimum ? {} : {minimum: config.minimum};

const getTypeSchema = (config: NumberSubsetConfig): CoreSchemaMetaSchema =>
    config.isInteger ? {type: 'integer'} : {type: 'number'};

export const numberSubsetConfigToJsonSchema = (config: NumberSubsetConfig): JsonSchema => ({
    ...getTypeSchema(config),
    ...getExclusiveMaximum(config),
    ...getExclusiveMinimum(config),
    ...getMaximum(config),
    ...getMinimum(config),
    ...getNotSchema(config)
});
