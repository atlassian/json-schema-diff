import {ArraySubsetConfig} from './array-subset-config';

const isEmptyItemsAndMinItemsContradiction = (config: ArraySubsetConfig): boolean => {
    const itemsAcceptsNoValues = config.items.type === 'empty';
    const minItemsRequiresValues = config.minItems > 0;
    return itemsAcceptsNoValues && minItemsRequiresValues;
};

const isMaxItemsAndMinItemsContradiction = (config: ArraySubsetConfig): boolean => config.minItems > config.maxItems;

const isMinItemsContradiction = (config: ArraySubsetConfig): boolean => config.minItems === Number.POSITIVE_INFINITY;

const isItemsAndNotItemsContradiction = (config: ArraySubsetConfig): boolean =>
    config.not.some((notConfig) => config.items.isSubsetOf(notConfig.items));

const contradictionTests = [
    isEmptyItemsAndMinItemsContradiction,
    isMaxItemsAndMinItemsContradiction,
    isMinItemsContradiction,
    isItemsAndNotItemsContradiction
];

export const arraySubsetConfigHasContradictions = (config: ArraySubsetConfig): boolean =>
    contradictionTests.some((contradictionTest) => contradictionTest(config));
