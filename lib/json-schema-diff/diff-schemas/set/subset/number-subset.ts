import {JsonSchema} from 'json-schema-spec-types';
import {Subset} from '../set';
import {complementNumberSubsetConfig} from './number-subset/complement-number-subset-config';
import {intersectNumberSubsetConfig} from './number-subset/intersect-number-subset-config';
import {NumberSubsetConfig} from './number-subset/number-subset-config';
import {numberSubsetConfigHasContradictions} from './number-subset/number-subset-config-has-contradictions';
import {numberSubsetConfigToJsonSchema} from './number-subset/number-subset-config-to-json-schema';
import {simplifyNumberSubsetConfig} from './number-subset/simplify-number-subset-config';
import {AllSubset, EmptySubset} from './subset';

class SomeNumberSubset implements Subset<'number'> {
    public readonly type = 'some';
    public readonly setType = 'number';
    public constructor(private readonly config: NumberSubsetConfig) {
    }

    public complement(): Array<Subset<'number'>> {
        return complementNumberSubsetConfig(this.config).map(createNumberSubsetFromConfig);
    }

    public intersect(other: Subset<'number'>): Subset<'number'> {
        return other.intersectWithSome(this);
    }

    public intersectWithSome(other: SomeNumberSubset): Subset<'number'> {
        return createNumberSubsetFromConfig(intersectNumberSubsetConfig(this.config, other.config));
    }

    public toJsonSchema(): JsonSchema {
        return numberSubsetConfigToJsonSchema(this.config);
    }
}

export const allNumberSubset = new AllSubset('number');
export const emptyNumberSubset = new EmptySubset('number');
export const createNumberSubsetFromConfig = (config: NumberSubsetConfig): Subset<'number'> => {
    const simplifiedConfig = simplifyNumberSubsetConfig(config);
    return numberSubsetConfigHasContradictions(simplifiedConfig)
        ? emptyNumberSubset
        : new SomeNumberSubset(simplifiedConfig);
};
