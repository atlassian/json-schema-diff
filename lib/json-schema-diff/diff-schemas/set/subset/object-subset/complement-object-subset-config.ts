import {allJsonSet, emptyJsonSet} from '../../json-set';
import {defaultMaxProperties, defaultMinProperties, defaultProperties, defaultRequired} from '../../keyword-defaults';
import {getPropertyNames, getPropertySet, ObjectSubsetConfig} from './object-subset-config';

const complementAdditionalProperties = (config: ObjectSubsetConfig): ObjectSubsetConfig => ({
    additionalProperties: allJsonSet,
    maxProperties: defaultMaxProperties,
    minProperties: defaultMinProperties,
    not: [{
        additionalProperties: config.additionalProperties,
        maxProperties: defaultMaxProperties,
        minProperties: defaultMinProperties,
        not: [],
        properties: config.properties,
        required: defaultRequired
    }],
    properties: defaultProperties,
    required: defaultRequired
});

const complementMaxProperties = (config: ObjectSubsetConfig): ObjectSubsetConfig => ({
    additionalProperties: allJsonSet,
    maxProperties: defaultMaxProperties,
    minProperties: config.maxProperties + 1,
    not: [],
    properties: defaultProperties,
    required: defaultRequired
});

const complementMinProperties = (config: ObjectSubsetConfig): ObjectSubsetConfig => ({
    additionalProperties: allJsonSet,
    maxProperties: config.minProperties - 1,
    minProperties: defaultMinProperties,
    not: [],
    properties: defaultProperties,
    required: defaultRequired
});

const complementNot = (config: ObjectSubsetConfig): ObjectSubsetConfig[] => config.not;

const complementProperties = (config: ObjectSubsetConfig): ObjectSubsetConfig[] =>
    getPropertyNames(config).map((propertyName) => {
        const complementedPropertySet = getPropertySet(config, propertyName).complement();

        return {
            additionalProperties: allJsonSet,
            maxProperties: defaultMaxProperties,
            minProperties: defaultMinProperties,
            not: [],
            properties: {[propertyName]: complementedPropertySet},
            required: [propertyName]
        };
    });

const complementRequiredProperties = (config: ObjectSubsetConfig): ObjectSubsetConfig[] =>
    config.required.map((requiredPropertyName) => ({
        additionalProperties: allJsonSet,
        maxProperties: defaultMaxProperties,
        minProperties: defaultMinProperties,
        not: [],
        properties: {
            [requiredPropertyName]: emptyJsonSet
        },
        required: defaultRequired
    }));

export const complementObjectSubsetConfig = (config: ObjectSubsetConfig): ObjectSubsetConfig[] =>
    [
        complementAdditionalProperties(config),
        complementMaxProperties(config),
        complementMinProperties(config),
        ...complementNot(config),
        ...complementProperties(config),
        ...complementRequiredProperties(config)
    ];
