import {getPropertySet, ObjectSubsetConfig} from './object-subset-config';

const isMinPropertiesBiggerThanDefinedProperties = (config: ObjectSubsetConfig): boolean => {
    const numberOfDefinedPropertiesInSchema = Object.values(config.properties)
        .filter((propertySchema) => propertySchema.type !== 'empty')
        .length;

    return config.minProperties > numberOfDefinedPropertiesInSchema;
};

const areAdditionalPropertiesNotAllowed = (config: ObjectSubsetConfig): boolean =>
    config.additionalProperties.type === 'empty';

const isMinPropertiesAndAdditionalPropertiesContradiction = (config: ObjectSubsetConfig): boolean =>
    areAdditionalPropertiesNotAllowed(config) && isMinPropertiesBiggerThanDefinedProperties(config);

const isRequiredAndPropertiesAndAdditionalPropertiesContradiction = (config: ObjectSubsetConfig): boolean =>
    config.required.some((propertyName) => getPropertySet(config, propertyName).type === 'empty');

const isMinPropertiesAndMaxPropertiesContradiction = (config: ObjectSubsetConfig): boolean =>
    config.minProperties > config.maxProperties;

const isMinPropertiesContradiction = (config: ObjectSubsetConfig): boolean =>
    config.minProperties === Number.POSITIVE_INFINITY;

const isMaxPropertiesAndRequiredContradiction = (config: ObjectSubsetConfig): boolean =>
    config.required.length > config.maxProperties;

const isAdditionalPropertiesAndNotAdditionalPropertiesContradiction = (config: ObjectSubsetConfig): boolean =>
    // TODO: When we support 'not', does this need to look at not.properties?
    config.not.some((notConfig) => config.additionalProperties.isSubsetOf(notConfig.additionalProperties));

const contradictionTests = [
    isRequiredAndPropertiesAndAdditionalPropertiesContradiction,
    isMinPropertiesAndAdditionalPropertiesContradiction,
    isMinPropertiesAndMaxPropertiesContradiction,
    isMinPropertiesContradiction,
    isMaxPropertiesAndRequiredContradiction,
    isAdditionalPropertiesAndNotAdditionalPropertiesContradiction
];

export const objectSubsetConfigHasContradictions = (config: ObjectSubsetConfig): boolean =>
    contradictionTests.some((contradictionTest) => contradictionTest(config));
