import {emptyJsonSet} from '../../json-set';
import {defaultMaxProperties} from '../../keyword-defaults';
import {ObjectSubsetConfig} from './object-subset-config';

const maxPropertiesAllowsNoProperties = (config: ObjectSubsetConfig): boolean => config.maxProperties === 0;

/*
All ObjectSubsetConfig objects go through this simplification when being created. The only way 2 or more entries can
appear inside the not array is when two existing ObjectSubsetConfigs are intersected, both of which are simplified
already. Therefore we only need to apply this simplification when config.not === 1.
*/
const notDisallowsObjectsWithNoProperties = (config: ObjectSubsetConfig): boolean =>
    config.not.length === 1
    && config.not[0].additionalProperties.type === 'empty'
    && Object.keys(config.not[0].properties).length === 0;

export const simplifyObjectSubsetConfig = (config: ObjectSubsetConfig): ObjectSubsetConfig => {
    if (maxPropertiesAllowsNoProperties(config)) {
        return {...config, maxProperties: defaultMaxProperties, additionalProperties: emptyJsonSet};
    }

    if (notDisallowsObjectsWithNoProperties(config)) {
        return {...config, not: [], minProperties: 1};
    }

    return config;
};
