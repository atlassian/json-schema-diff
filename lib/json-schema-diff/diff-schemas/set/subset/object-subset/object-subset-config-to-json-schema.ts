import {CoreSchemaMetaSchema, JsonSchema, JsonSchemaMap} from 'json-schema-spec-types';
import {defaultMaxProperties, defaultMinProperties} from '../../keyword-defaults';
import {addAnyOfIfNecessary} from '../../set';
import {ObjectSubsetConfig, ParsedPropertiesKeyword} from './object-subset-config';

const getAdditionalPropertiesSchema = (config: ObjectSubsetConfig): CoreSchemaMetaSchema =>
    config.additionalProperties.type === 'all'
        ? {}
        : {additionalProperties: config.additionalProperties.toJsonSchema()};

const getMaxPropertiesSchema = (config: ObjectSubsetConfig): CoreSchemaMetaSchema =>
    config.maxProperties === defaultMaxProperties ? {} : {maxProperties: config.maxProperties};

const getMinPropertiesSchema = (config: ObjectSubsetConfig): CoreSchemaMetaSchema =>
    config.minProperties === defaultMinProperties ? {} : {minProperties: config.minProperties};

const getNotSchema = (config: ObjectSubsetConfig): CoreSchemaMetaSchema =>
    config.not.length === 0 ? {} : {not: addAnyOfIfNecessary(config.not.map(objectSubsetConfigToJsonSchema))};

const toSchemaProperties = (properties: ParsedPropertiesKeyword): JsonSchemaMap => {
    const schemaProperties: JsonSchemaMap = {};

    for (const propertyName of Object.keys(properties)) {
        schemaProperties[propertyName] = properties[propertyName].toJsonSchema();
    }

    return schemaProperties;
};

const getPropertiesSchema = (config: ObjectSubsetConfig): CoreSchemaMetaSchema =>
    Object.keys(config.properties).length === 0 ? {} : {properties: toSchemaProperties(config.properties)};

const getRequiredSchema = (config: ObjectSubsetConfig): CoreSchemaMetaSchema =>
    config.required.length === 0 ? {} : {required: config.required};

export const objectSubsetConfigToJsonSchema = (config: ObjectSubsetConfig): JsonSchema => ({
    ...getAdditionalPropertiesSchema(config),
    ...getMaxPropertiesSchema(config),
    ...getMinPropertiesSchema(config),
    ...getNotSchema(config),
    ...getPropertiesSchema(config),
    ...getRequiredSchema(config),
    type: 'object'
});
