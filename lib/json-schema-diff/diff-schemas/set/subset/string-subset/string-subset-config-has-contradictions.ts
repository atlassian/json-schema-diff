import {StringSubsetConfig} from './string-subset-config';

const isMaxLengthAndMinLengthContradiction = (config: StringSubsetConfig): boolean =>
    config.minLength > config.maxLength;

const isMinLengthContradiction = (config: StringSubsetConfig): boolean => config.minLength === Number.POSITIVE_INFINITY;

type contradictionTestFn = (config: StringSubsetConfig) => boolean;

const contradictionTests: contradictionTestFn[] = [
    isMaxLengthAndMinLengthContradiction,
    isMinLengthContradiction
];

export const stringSubsetConfigHasContradictions = (config: StringSubsetConfig): boolean =>
    contradictionTests.some((contradictionTest) => contradictionTest(config));
