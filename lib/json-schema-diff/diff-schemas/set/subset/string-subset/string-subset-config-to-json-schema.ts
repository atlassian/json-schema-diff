import {CoreSchemaMetaSchema, JsonSchema} from 'json-schema-spec-types';
import {defaultMaxLength, defaultMinLength} from '../../keyword-defaults';
import {StringSubsetConfig} from './string-subset-config';

const getMaxLengthSchema = (config: StringSubsetConfig): CoreSchemaMetaSchema =>
    config.maxLength === defaultMaxLength ? {} : {maxLength: config.maxLength};

const getMinLengthSchema = (config: StringSubsetConfig): CoreSchemaMetaSchema =>
    config.minLength === defaultMinLength ? {} : {minLength: config.minLength};

export const stringSubsetConfigToJsonSchema = (config: StringSubsetConfig): JsonSchema => ({
    type: 'string',
    ...getMaxLengthSchema(config),
    ...getMinLengthSchema(config)
});
