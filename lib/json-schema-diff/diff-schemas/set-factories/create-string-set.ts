import {SimpleTypes} from 'json-schema-spec-types';
import {defaultMaxLength, defaultMinLength} from '../set/keyword-defaults';
import {Set, Subset} from '../set/set';
import {allStringSubset, createStringSubsetFromConfig, emptyStringSubset} from '../set/subset/string-subset';
import {createTypeSetFromSubsets} from '../set/type-set';
import {isTypeSupported} from './is-type-supported';
import {ParseContext} from '../../config';

export interface StringSetParsedKeywords {
    maxLength: number;
    minLength: number;
    type: SimpleTypes[];
}

const supportsAllStrings = (stringSetParsedKeywords: StringSetParsedKeywords): boolean =>
    stringSetParsedKeywords.maxLength === defaultMaxLength
    && stringSetParsedKeywords.minLength === defaultMinLength;

const createStringSubset = (stringSetParsedKeywords: StringSetParsedKeywords): Subset<'string'> => {
    if (!isTypeSupported(stringSetParsedKeywords.type, 'string')) {
        return emptyStringSubset;
    }

    if (supportsAllStrings(stringSetParsedKeywords)) {
        return allStringSubset;
    }

    return createStringSubsetFromConfig({...stringSetParsedKeywords});
};

export const createStringSet = (stringSetParsedKeywords: StringSetParsedKeywords, context: ParseContext): Set<'string'> => {
    const stringSubset = createStringSubset(stringSetParsedKeywords);
    return createTypeSetFromSubsets('string', [stringSubset], context);
};
