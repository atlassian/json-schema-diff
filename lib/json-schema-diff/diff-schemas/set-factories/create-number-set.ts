import {SimpleTypes} from 'json-schema-spec-types';
import {defaultMaximum, defaultMinimum} from '../set/keyword-defaults';
import {Set, Subset} from '../set/set';
import {allNumberSubset, createNumberSubsetFromConfig, emptyNumberSubset} from '../set/subset/number-subset';
import {createTypeSetFromSubsets} from '../set/type-set';
import {isTypeSupported} from './is-type-supported';
import {ParseContext} from '../../config';

export interface NumberSetParsedKeywords {
    type: SimpleTypes[];
    exclusiveMaximum: number;
    exclusiveMinimum: number;
    maximum: number;
    minimum: number;
}

// eslint-disable-next-line complexity
const supportsAllNumbers = (numberSetParsedKeywords: NumberSetParsedKeywords): boolean =>
    isTypeSupported(numberSetParsedKeywords.type, 'number')
    && numberSetParsedKeywords.maximum === defaultMaximum
    && numberSetParsedKeywords.minimum === defaultMinimum
    && numberSetParsedKeywords.exclusiveMaximum === defaultMaximum
    && numberSetParsedKeywords.exclusiveMinimum === defaultMinimum;

const supportsNoNumbers = (numberSetParsedKeywords: NumberSetParsedKeywords): boolean =>
    !isTypeSupported(numberSetParsedKeywords.type, 'number') && !isTypeSupported(numberSetParsedKeywords.type, 'integer');

const createNumberSubset = (numberSetParsedKeywords: NumberSetParsedKeywords): Subset<'number'> => {
    if (supportsNoNumbers(numberSetParsedKeywords)) {
        return emptyNumberSubset;
    }

    if (supportsAllNumbers(numberSetParsedKeywords)) {
        return allNumberSubset;
    }

    const isInteger = !isTypeSupported(numberSetParsedKeywords.type, 'number');
    return createNumberSubsetFromConfig({
        exclusiveMaximum: numberSetParsedKeywords.exclusiveMaximum,
        exclusiveMinimum: numberSetParsedKeywords.exclusiveMinimum,
        maximum: numberSetParsedKeywords.maximum,
        minimum: numberSetParsedKeywords.minimum,
        isInteger,
        not: []
    });
};

export const createNumberSet = (numberSetParsedKeywords: NumberSetParsedKeywords, context: ParseContext): Set<'number'> => {
    const numberSubset = createNumberSubset(numberSetParsedKeywords);
    return createTypeSetFromSubsets('number', [numberSubset], context);
};
