import {SimpleTypes} from 'json-schema-spec-types';
import {allJsonSet, createJsonSetFromConfig, emptyJsonSet} from '../set/json-set';
import {Set} from '../set/set';
import {ParsedPropertiesKeyword} from '../set/subset/object-subset/object-subset-config';
import {createArraySet} from './create-array-set';
import {createNumberSet} from './create-number-set';
import {createObjectSet} from './create-object-set';
import {createStringSet} from './create-string-set';
import {createTypeSet} from './create-type-set';
import {ParseContext} from '../../config';

export interface ParsedSchemaKeywords {
    additionalProperties: Set<'json'>;
    exclusiveMaximum: number;
    exclusiveMinimum: number;
    items: Set<'json'>;
    maximum: number;
    minimum: number;
    maxItems: number;
    maxLength: number;
    maxProperties: number;
    minItems: number;
    minLength: number;
    minProperties: number;
    properties: ParsedPropertiesKeyword;
    required: string[];
    type: SimpleTypes[];
}

export const createJsonSetFromParsedSchemaKeywords = (parsedSchemaKeywords: ParsedSchemaKeywords, context: ParseContext): Set<'json'> =>
    createJsonSetFromConfig({
        array: createArraySet(parsedSchemaKeywords, context),
        boolean: createTypeSet('boolean', parsedSchemaKeywords.type, context),
        null: createTypeSet('null', parsedSchemaKeywords.type, context),
        number: createNumberSet(parsedSchemaKeywords, context),
        object: createObjectSet(parsedSchemaKeywords, context),
        string: createStringSet(parsedSchemaKeywords, context)
    });

export const createAllJsonSet = (): Set<'json'> => allJsonSet;

export const createEmptyJsonSet = (): Set<'json'> => emptyJsonSet;
