import {SimpleTypes} from 'json-schema-spec-types';
import * as _ from 'lodash';
import {defaultMaxProperties, defaultMinProperties, defaultRequired} from '../set/keyword-defaults';
import {Set, Subset} from '../set/set';
import {allObjectSubset, createObjectSubsetFromConfig, emptyObjectSubset} from '../set/subset/object-subset';
import {ParsedPropertiesKeyword} from '../set/subset/object-subset/object-subset-config';
import {createTypeSetFromSubsets} from '../set/type-set';
import {isTypeSupported} from './is-type-supported';
import {ParseContext} from '../../config';

export interface ObjectSetParsedKeywords {
    additionalProperties: Set<'json'>;
    maxProperties: number;
    minProperties: number;
    properties: ParsedPropertiesKeyword;
    required: string[];
    type: SimpleTypes[];
}

/* eslint-disable-next-line complexity */
const supportsAllObjects = (objectSetParsedKeywords: ObjectSetParsedKeywords): boolean => {
    const everyPropertyIsAll = Object.values(objectSetParsedKeywords.properties)
        .every((propertySet) => propertySet.type === 'all');

    return everyPropertyIsAll
        && _.isEqual(objectSetParsedKeywords.required, defaultRequired)
        && objectSetParsedKeywords.additionalProperties.type === 'all'
        && objectSetParsedKeywords.minProperties === defaultMinProperties
        && objectSetParsedKeywords.maxProperties === defaultMaxProperties;
};

const createObjectSubset = (objectSetParsedKeywords: ObjectSetParsedKeywords): Subset<'object'> => {
    if (!isTypeSupported(objectSetParsedKeywords.type, 'object')) {
        return emptyObjectSubset;
    }

    if (supportsAllObjects(objectSetParsedKeywords)) {
        return allObjectSubset;
    }

    return createObjectSubsetFromConfig({...objectSetParsedKeywords, not: []});
};

export const createObjectSet = (objectSetParsedKeywords: ObjectSetParsedKeywords, context: ParseContext): Set<'object'> => {
    const objectSubset = createObjectSubset(objectSetParsedKeywords);
    return createTypeSetFromSubsets('object', [objectSubset], context);
};
