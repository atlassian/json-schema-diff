import {CoreSchemaMetaSchema, JsonSchema, JsonSchemaMap, SimpleTypes} from 'json-schema-spec-types';
import {
    createAllJsonSet,
    createEmptyJsonSet,
    createJsonSetFromParsedSchemaKeywords
} from './set-factories/create-json-set';
import {
    defaultMaximum,
    defaultMaxItems,
    defaultMaxLength,
    defaultMaxProperties,
    defaultMinimum,
    defaultMinItems,
    defaultMinLength,
    defaultMinProperties,
    defaultRequired,
    defaultTypes
} from './set/keyword-defaults';
import {Set} from './set/set';
import {ParsedPropertiesKeyword} from './set/subset/object-subset/object-subset-config';
import {ParseContext} from '../config';


class JsonToSetParser {
    public constructor(private readonly context: ParseContext) {}

    public parse(schema: JsonSchema): Set<'json'> {
        return this.parseSchemaOrUndefinedAsJsonSet(schema);
    }

    private parseSchemaOrUndefinedAsJsonSet(schema: JsonSchema | undefined): Set<'json'> {
        return (typeof schema === 'boolean' || schema === undefined)
            ? this.parseBooleanSchema(schema)
            : this.parseCoreSchemaMetaSchema(schema);
    }

    private parseBooleanSchema(schema: boolean | undefined): Set<'json'> {
        const allowsAllJsonValues = schema === undefined ? true : schema;
        return allowsAllJsonValues ? createAllJsonSet() : createEmptyJsonSet();
    };

    private parseCoreSchemaMetaSchema(schema: CoreSchemaMetaSchema): Set<'json'> {
        const typeKeywordsSet = this.parseTypeKeywords(schema);
        const booleanLogicKeywordSets = this.parseBooleanLogicKeywords(schema);
        return booleanLogicKeywordSets.reduce((accumulator, set) => accumulator.intersect(set), typeKeywordsSet);
    };

    private parseTypeKeywords(schema: CoreSchemaMetaSchema): Set<'json'> {
        return createJsonSetFromParsedSchemaKeywords({
            additionalProperties: this.parseSchemaOrUndefinedAsJsonSet(schema.additionalProperties),
            exclusiveMaximum: this.parseNumericKeyword(schema.exclusiveMaximum, defaultMaximum),
            exclusiveMinimum: this.parseNumericKeyword(schema.exclusiveMinimum, defaultMinimum),
            items: this.parseSchemaOrUndefinedAsJsonSet(schema.items),
            maximum: this.parseNumericKeyword(schema.maximum, defaultMaximum),
            maxItems: this.parseNumericKeyword(schema.maxItems, defaultMaxItems),
            maxLength: this.parseNumericKeyword(schema.maxLength, defaultMaxLength),
            maxProperties: this.parseNumericKeyword(schema.maxProperties, defaultMaxProperties),
            minimum: this.parseNumericKeyword(schema.minimum, defaultMinimum),
            minItems: this.parseNumericKeyword(schema.minItems, defaultMinItems),
            minLength: this.parseNumericKeyword(schema.minLength, defaultMinLength),
            minProperties: this.parseNumericKeyword(schema.minProperties, defaultMinProperties),
            properties: this.parseSchemaProperties(schema.properties),
            required: this.parseRequiredKeyword(schema.required),
            type: this.parseType(schema.type)
        }, this.context);
    }

    private parseBooleanLogicKeywords(schema: CoreSchemaMetaSchema): Array<Set<'json'>> {
        return [
            ...this.parseAnyOfKeyword(schema.anyOf),
            ...this.parseAllOfKeyword(schema.allOf),
            ...this.parseNotKeyword(schema.not),
            ...this.parseOneOfKeyword(schema.oneOf)
        ];
    }

    private parseAnyOfKeyword(anyOfSchemas: JsonSchema[] | undefined): Array<Set<'json'>> {
        if (!anyOfSchemas) {
            return [];
        }

        const parsedAnyOfSet = anyOfSchemas
            .map((schema) => this.parseSchemaOrUndefinedAsJsonSet(schema))
            .reduce((accumulator, set) => accumulator.union(set));
        return [parsedAnyOfSet];
    };

    private parseAllOfKeyword(allOfSchemas: JsonSchema[] | undefined): Array<Set<'json'>> {
        return (allOfSchemas || []).map((schema) => this.parseSchemaOrUndefinedAsJsonSet(schema));
    }

    private parseNotKeyword(notSchema: JsonSchema | undefined): Array<Set<'json'>> {
        if (!notSchema) {
            return [];
        }

        return [this.parseSchemaOrUndefinedAsJsonSet(notSchema).complement()];
    };

    private parseOneOfKeyword(oneOfSchemas: JsonSchema[] | undefined): Array<Set<'json'>> {
        if (!oneOfSchemas) {
            return [];
        }

        // oneOf: [A, B, C] = (A && !B && !C) || (!A && B && !C) || (!A && !B && C)

        const parsedSchemaList = oneOfSchemas.map((schema) => this.parseSchemaOrUndefinedAsJsonSet(schema));
        const complementedSchemas = parsedSchemaList.map((schema) => schema.complement());
        const parsedOneOfSchema = parsedSchemaList
            .map((schema, i) => [...complementedSchemas.slice(0, i), schema, ...complementedSchemas.slice(i + 1)])
            .map((schemaGroup) => schemaGroup.reduce((acc, schema) => acc.intersect(schema)))
            .reduce((acc, schemaGroup) => acc.union(schemaGroup));

        return [parsedOneOfSchema];
    }

    private parseSchemaProperties(schemaProperties: JsonSchemaMap = {}): ParsedPropertiesKeyword {
        const objectSetProperties: ParsedPropertiesKeyword = {};

        for (const propertyName of Object.keys(schemaProperties)) {
            const propertySchema = schemaProperties[propertyName];
            objectSetProperties[propertyName] = this.parseSchemaOrUndefinedAsJsonSet(propertySchema);
        }
        return objectSetProperties;
    };

    private parseNumericKeyword(keywordValue: number | undefined, defaultValue: number): number {
        return typeof keywordValue === 'number' ? keywordValue : defaultValue;
    }

    private parseRequiredKeyword(required: string[] | undefined): string[] {
        return required || defaultRequired;
    }

    private parseType(type: SimpleTypes | SimpleTypes[] | undefined): SimpleTypes[] {
        if (!type) {
            return defaultTypes;
        }

        return typeof type === 'string' ? [type] : type;
    };
}

export const parseAsJsonSet = (schema: JsonSchema, context: ParseContext): Set<'json'> =>
    new JsonToSetParser(context).parse(schema);
