import * as convict from 'convict';

export interface ParseContext {
    readonly heuristics: {
        readonly applyLawOfAbsorptionInTypeSetCartesianProduct: boolean
    }
}

export interface Config {
    parseContext: ParseContext;
}

const createConvictInstance = (env: NodeJS.Dict<string>): convict.Config<Config> => convict<Config>({
    parseContext: {
        heuristics: {
            applyLawOfAbsorptionInTypeSetCartesianProduct: {
                default: true,
                env: 'JSON_SCHEMA_DIFF_APPLY_ABSORPTION_IN_CARTESIAN_PRODUCT',
                format: Boolean
            }
        }
    }
}, {env});


export const loadConfig = (env: NodeJS.Dict<string>): Readonly<Config> => {
    const config = createConvictInstance(env);
    config.validate({allowed: 'strict'});
    return config.getProperties();
};