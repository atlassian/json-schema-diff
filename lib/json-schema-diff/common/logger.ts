import {Set} from '../diff-schemas/set/set';

export type SetLogger = <T> (setName: string, set: Set<T>) => void;

export const logSetDebug: SetLogger = <T> (setName: string, set: Set<T>) => {
    if (process.env.JSON_SCHEMA_DIFF_ENABLE_DEBUG === 'true') {
        console.log(`\n${setName}`);
        console.log(JSON.stringify(set.toJsonSchema(), null, 2));
    }
};
