# Json Schema Diff Supported Keywords

This is a list of all the keywords in json schema and which keywords Json Schema Diff currently supports.

## Validation Keywords for Any Instance Type

| Keyword | Supported |
|---|---|
| type | yes |
| enum | no |
| const | no |
| default | no |

## Validation Keywords for Numeric Instances (number and integer)

| Keyword | Supported |
|---|---|
| multipleOf | no |
| maximum | yes |
| exclusiveMaximum | yes |
| minimum | yes |
| exclusiveMinimum | yes |

## Validation Keywords for Strings

| Keyword | Supported |
|---|---|
| format | no |
| maxLength | yes |
| minLength | yes |
| pattern | no |

## Validation Keywords for Arrays

| Keyword | Supported |
|---|---|
| items | partial, yes for a single schema, no for an array of schemas |
| additionalItems | no |
| maxItems | yes |
| minItems | yes |
| uniqueItems | no |
| contains | no |


## Validation Keywords for Objects

| Keyword | Supported |
|---|---|
| maxProperties | yes |
| minProperties | yes |
| required | yes |
| properties | yes |
| patternProperties | no |
| additionalProperties | yes |
| dependencies | no |
| propertyNames | no |

## Keywords for Applying Subschemas Conditionally

| Keyword | Supported |
|---|---|
| if | no |
| then | no |
| else | no |


## Keywords for Applying Subschemas With Boolean Logic

| Keyword | Supported |
|---|---|
| allOf | yes |
| anyOf | yes |
| oneOf | yes, with limitations - see below for details |
| not | yes |

### Limitions in the oneOf support

Nesting oneOf keywords inside each other requires an exponentially increasing amount of logical operations in order to calculate the difference. It is expected that the tool will become unusuably slow when given schemas containing many levels of nested oneOf keywords, for example:

```
{
    "oneOf": [
        {
            "oneOf": [
                {
                    "oneOf": [
                        { 
                            "oneOf": [...] 
                        },
                        ...
                    ]
                },
                ...
            ]
        },
        ...
    ]
}
```

During testing we've found the tool becomes unusable at around 4 levels of nesting, however your milage may vary.

## Miscellaneous Keywords

| Keyword | Supported |
|---|---|
| $ref | partial, yes for internal non-circular references, no for external references, no for circular references |
