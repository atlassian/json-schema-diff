### [0.18.1](https://bitbucket.org/atlassian/json-schema-diff/compare/0.18.0...0.18.1) (2024-03-20)

## [0.18.0](https://bitbucket.org/atlassian/json-schema-diff/compare/0.17.1...0.18.0) (2022-08-15)


### ⚠ BREAKING CHANGES

* NodeJS v8.x and v10.x are not longer supported. Migrate to NodeJS v12.x or above

### Features

* allow enabling/disabling law of absorption simplication in cartesian product ([f83cce0](https://bitbucket.org/atlassian/json-schema-diff/commit/f83cce0a6796842445dea3d82586ab8827ded99c))


### Reverts

* Revert "fix: remove unnecessary and non-performant isSubsetOf operations when calculating cartesian product of type sets" ([345c464](https://bitbucket.org/atlassian/json-schema-diff/commit/345c464749fc779a6a9e69194903e220ebb5ff36))


### Miscellaneous Chores

* bump dependencies and migrate from deprecated tslint to eslint ([739d5e1](https://bitbucket.org/atlassian/json-schema-diff/commit/739d5e181f1cf15ca8aeb5fb39bb46f0c7469c15))

### [0.17.1](https://bitbucket.org/atlassian/json-schema-diff/compare/0.17.0...0.17.1) (2021-10-11)


### Bug Fixes

* remove unnecessary and non-performant isSubsetOf operations when calculating cartesian product of type sets ([2e0a41f](https://bitbucket.org/atlassian/json-schema-diff/commit/2e0a41fecf18cc3bd95aa21a52d7fbb276e6dd07)), closes [#5](https://bitbucket.org/atlassian/json-schema-diff/issues/5)

## [0.17.0](https://bitbucket.org/atlassian/json-schema-diff/compare/0.16.2...0.17.0) (2021-06-28)


### Features

* add support for minimum, exclusiveMinumum, maximum and exclusiveMaximum ([796c36a](https://bitbucket.org/atlassian/json-schema-diff/commit/796c36a42ba9499cf9a6e36b1a06ab4b42f51670))
* consider integer as a subtype of number ([3f6d42b](https://bitbucket.org/atlassian/json-schema-diff/commit/3f6d42b0553dea8f4b5a5ec0969d3673de8ae0f5))
* dropping support for node 8 ([266b3ec](https://bitbucket.org/atlassian/json-schema-diff/commit/266b3ec65a338c4c05d084df96a42da84b676099))

<a name="0.16.0"></a>
# [0.16.0](https://bitbucket.org/atlassian/json-schema-diff/compare/0.15.0...0.16.0) (2020-08-14)


### Bug Fixes

* issue when schema property names matched functions in object.prototype ([50e2fa3](https://bitbucket.org/atlassian/json-schema-diff/commits/50e2fa3)), closes [#2](https://bitbucket.org/atlassian/json-schema-diff/issue/2)


### Features

* add support for string maxLength and minLength ([cc41c09](https://bitbucket.org/atlassian/json-schema-diff/commits/cc41c09))



<a name="0.15.0"></a>
# [0.15.0](https://bitbucket.org/atlassian/json-schema-diff/compare/0.14.0...0.15.0) (2020-01-24)


### Features

* add support for the not keyword ([4bd65e4](https://bitbucket.org/atlassian/json-schema-diff/commits/4bd65e4))
* add support for the oneOf keyword ([6b6c589](https://bitbucket.org/atlassian/json-schema-diff/commits/6b6c589))
* output schemas only contain a type array when multiple types are present ([ba422f6](https://bitbucket.org/atlassian/json-schema-diff/commits/ba422f6))



<a name="0.14.0"></a>
# [0.14.0](https://bitbucket.org/atlassian/json-schema-diff/compare/0.13.0...0.14.0) (2020-01-22)


### Features

* dropping support for node 6 ([ea07554](https://bitbucket.org/atlassian/json-schema-diff/commits/ea07554))


### BREAKING CHANGES

* Removed support for node 6. To migrate use node version 8.x or higher.



<a name="0.13.0"></a>
# [0.13.0](https://bitbucket.org/atlassian/json-schema-diff/compare/0.12.1...0.13.0) (2019-08-19)


### Bug Fixes

* correct repository URL in package.json ([a99f3a5](https://bitbucket.org/atlassian/json-schema-diff/commits/a99f3a5))


### Features

* support for the anyOf keyword ([f17c20e](https://bitbucket.org/atlassian/json-schema-diff/commits/f17c20e))



<a name="0.12.1"></a>
## [0.12.1](https://bitbucket.org/atlassian/json-schema-diff/compare/0.12.0...0.12.1) (2019-04-15)


### Bug Fixes

* add missing values to the object complement schema calculation ([9d228e1](https://bitbucket.org/atlassian/json-schema-diff/commits/9d228e1))
* add missing values to the array complement schema calculation ([8869982](https://bitbucket.org/atlassian/json-schema-diff/commits/8869982))



<a name="0.12.0"></a>
# [0.12.0](https://bitbucket.org/atlassian/json-schema-diff/compare/0.11.0...0.12.0) (2019-04-09)


### Bug Fixes

* generate a complete difference when schemas with both additionalProperties and properties are changed ([56b0149](https://bitbucket.org/atlassian/json-schema-diff/commits/56b0149))


### Features

* add support for the allOf keyword ([37513a5](https://bitbucket.org/atlassian/json-schema-diff/commits/37513a5))
* add support for the maxItems keyword ([770b511](https://bitbucket.org/atlassian/json-schema-diff/commits/770b511))
* add support for the maxProperties keyword ([dfe643b](https://bitbucket.org/atlassian/json-schema-diff/commits/dfe643b))
* add support for the minProperties keyword ([a5f428f](https://bitbucket.org/atlassian/json-schema-diff/commits/a5f428f))



<a name="0.11.0"></a>
# [0.11.0](https://bitbucket.org/atlassian/json-schema-diff/compare/0.10.0...0.11.0) (2019-03-01)


### Features

* add support for minItems keyword ([af7ef8f](https://bitbucket.org/atlassian/json-schema-diff/commits/af7ef8f))



<a name="0.10.0"></a>
# [0.10.0](https://bitbucket.org/atlassian/json-schema-diff/compare/0.9.0...0.10.0) (2019-02-28)


### Features

* add support for the array items keyword ([6cc5f9e](https://bitbucket.org/atlassian/json-schema-diff/commits/6cc5f9e))



<a name="0.9.0"></a>
# [0.9.0](https://bitbucket.org/atlassian/json-schema-diff/compare/0.8.3...0.9.0) (2019-02-07)


### Features

* add support for required keyword ([2290b74](https://bitbucket.org/atlassian/json-schema-diff/commits/2290b74))


### BREAKING CHANGES

* Prior to this change the nodejs api returned added and removed differences detected as as an array of DiffResultDifference objects. Now the added and removed differences are returned as two seperate JsonSchema objects. In addition all the property names of the DiffResult object have been renamed. To migrate, update code that consumes the DiffResult object returned by the API to the new property names and types.



<a name="0.8.3"></a>
## [0.8.3](https://bitbucket.org/atlassian/json-schema-diff/compare/0.8.2...0.8.3) (2019-01-02)


### Bug Fixes

* fix race condition in the watch loop for unit tests ([5753cab](https://bitbucket.org/atlassian/json-schema-diff/commits/5753cab))



<a name="0.8.2"></a>
## [0.8.2](https://bitbucket.org/atlassian/json-schema-diff/compare/0.8.1...0.8.2) (2018-08-08)


### Bug Fixes

* declare lodash as production dependency ([2e7aa69](https://bitbucket.org/atlassian/json-schema-diff/commits/2e7aa69))



<a name="0.8.1"></a>
## [0.8.1](https://bitbucket.org/atlassian/json-schema-diff/compare/0.8.0...0.8.1) (2018-07-17)


### Bug Fixes

* throw an error when schema contains circular references instead of overflowing the stack ([60d4db1](https://bitbucket.org/atlassian/json-schema-diff/commits/60d4db1))



<a name="0.8.0"></a>
# [0.8.0](https://bitbucket.org/atlassian/json-schema-diff/compare/0.7.0...0.8.0) (2018-07-16)


### Features

* changed public api to return locations as arrays instead of strings ([f86a5dd](https://bitbucket.org/atlassian/json-schema-diff/commits/f86a5dd))



<a name="0.7.0"></a>
# [0.7.0](https://bitbucket.org/atlassian/json-schema-diff/compare/0.6.0...0.7.0) (2018-07-12)


### Features

* added public programmable api and removed support for allOf, anyOf and not keywords ([0c15017](https://bitbucket.org/atlassian/json-schema-diff/commits/0c15017))



<a name="0.6.0"></a>
# [0.6.0](https://bitbucket.org/atlassian/json-schema-diff/compare/0.5.0...0.6.0) (2018-05-23)


### Features

* add support for json schema references ([76a0457](https://bitbucket.org/atlassian/json-schema-diff/commits/76a0457))



<a name="0.5.0"></a>
# [0.5.0](https://bitbucket.org/atlassian/json-schema-diff/compare/0.4.0...0.5.0) (2018-05-23)


### Features

* add support for boolean schemas ([519f648](https://bitbucket.org/atlassian/json-schema-diff/commits/519f648))
* add support for properties and additionalProperties ([f68a38a](https://bitbucket.org/atlassian/json-schema-diff/commits/f68a38a))



<a name="0.4.0"></a>
# [0.4.0](https://bitbucket.org/atlassian/json-schema-diff/compare/0.3.0...0.4.0) (2018-05-03)


### Features

* add support for anyOf keyword ([1753af1](https://bitbucket.org/atlassian/json-schema-diff/commits/1753af1))



<a name="0.3.0"></a>
# [0.3.0](https://bitbucket.org/atlassian/json-schema-diff/compare/0.2.0...0.3.0) (2018-05-02)


### Features

* add support for the not keyword ([c9b7e98](https://bitbucket.org/atlassian/json-schema-diff/commits/c9b7e98))



<a name="0.2.0"></a>
# [0.2.0](https://bitbucket.org/atlassian/json-schema-diff/compare/0.1.0...0.2.0) (2018-04-23)


### Features

* add support for the allOf keyword ([e329acd](https://bitbucket.org/atlassian/json-schema-diff/commits/e329acd))



<a name="0.1.0"></a>
# [0.1.0](https://bitbucket.org/atlassian/json-schema-diff/compare/0.0.1...0.1.0) (2018-04-19)


### Features

* add check for type ([1c23987](https://bitbucket.org/atlassian/json-schema-diff/commits/1c23987))



