'use strict';

const gulp = require('gulp');

const bump = require('gulp-bump');
const clean = require('gulp-clean');
const colors = require('ansi-colors');
const conventionalChangelog = require('gulp-conventional-changelog');
const exec = require('child_process').exec;
const fs = require('fs');
const git = require('gulp-git');
const minimist = require('minimist');
const ts = require('gulp-typescript');
const eslint = require('gulp-eslint');

const options = minimist(process.argv.slice(2), {strings: ['type']});
const tsProjectBuildOutput = ts.createProject('tsconfig.json', {noEmit: false});
const unitTests = 'build-output/test/unit/**/*.spec.js';
const e2eTests = 'build-output/test/e2e/**/*.spec.js';

const utilities = {
    compileBuildOutput: () => {
        const tsResult = tsProjectBuildOutput.src().pipe(tsProjectBuildOutput());
        return tsResult.js.pipe(gulp.dest('build-output'));
    },
    exec: (command) => {
        return new Promise((resolve, reject) => {
            console.log(`Executing command '${colors.yellow(command)}'`);
            const childProcess = exec(command, (error) => {
                if (error) {
                    reject(error);
                } else {
                    resolve();
                }
            });
            childProcess.stdout.pipe(process.stdout);
            childProcess.stderr.pipe(process.stderr);
        });
    },
    getBumpType: () => {
        const validTypes = ['major', 'minor', 'patch', 'prerelease'];
        if (validTypes.indexOf(options.type) === -1) {
            throw new Error(
                `You must specify a release type as one of (${validTypes.join(', ')}), e.g. "--type minor"`
            );
        }
        return options.type;
    },
    getPackageJsonVersion: () => {
        return JSON.parse(fs.readFileSync('./package.json', 'utf8')).version;
    }
};

const bumpVersion = () =>
    gulp.src(['./package.json'])
        .pipe(bump({type: utilities.getBumpType()}))
        .pipe(gulp.dest('./'));

const cleanBuildOutput = () =>
    gulp.src('build-output', {force: true, read: false, allowEmpty: true}).pipe(clean());

const cleanDist = () =>
    gulp.src('dist', {force: true, read: false, allowEmpty: true}).pipe(clean());

const changelog = () => {
    return gulp.src('CHANGELOG.md')
        .pipe(conventionalChangelog({ preset: 'conventionalcommits'}))
        .pipe(gulp.dest('./'));
};

const commitRelease = () =>
    gulp.src('.')
        .pipe(git.add({args: '--all'}))
        .pipe(git.commit(`chore: release ${utilities.getPackageJsonVersion()}`));

const test = () => utilities.exec(`npx jasmine --color "${unitTests}" "${e2eTests}"`);

const testUnit = () => utilities.exec(`npx jasmine --color "${unitTests}"`);

const testE2e = () => utilities.exec(`npx jasmine --color "${e2eTests}"`);

const copyBuildOutputPackageJson = () =>
    gulp.src('package.json').pipe(gulp.dest('build-output'));

const compileDist = () => {
    const tsProjectDist = ts.createProject('tsconfig.json', {noEmit: false});
    const tsResult = gulp.src('lib/**/*.ts').pipe(tsProjectDist());
    return tsResult.js.pipe(gulp.dest('dist'));
};

const createNewTag = (callback) => {
    const version = utilities.getPackageJsonVersion();
    git.tag(version, `Created Tag for version: ${version}`, callback);
};

const lintCommits = () =>
    utilities.exec('./node_modules/.bin/commitlint --from=HEAD~20');

const lintTypescript = () =>
    tsProjectBuildOutput.src()
        .pipe(eslint())
        .pipe(eslint.format())
        .pipe(eslint.failAfterError());

const npmPublish = () => utilities.exec('npm publish');

const pushChanges = (callback) => {
    git.push('origin', 'master', {args: '--tags'}, callback);
};

const watchAndRunUnitTests = () => {
    gulp.watch(['lib/**/*.ts', 'test/**/*.ts'], {ignoreInitial: false}, gulp.series(utilities.compileBuildOutput, testUnit))
};

exports.default = gulp.parallel(
    gulp.series(
        cleanBuildOutput,
        gulp.parallel(copyBuildOutputPackageJson, utilities.compileBuildOutput),
        test
    ),
    gulp.parallel(lintTypescript, lintCommits)
);

exports.release = gulp.series(
    exports.default,
    cleanDist,
    compileDist,
    bumpVersion,
    changelog,
    commitRelease,
    createNewTag,
    pushChanges,
    npmPublish
);

exports.watch = gulp.series(
    cleanBuildOutput,
    copyBuildOutputPackageJson,
    watchAndRunUnitTests
);

exports.watchE2e = () => {
    const e2eFiles = ['build-output/lib/**/*', 'build-output/test/e2e/**/*', 'test/e2e/**/*.json'];
    gulp.watch(e2eFiles, {ignoreInitial: false}, testE2e);
};
