import {DiffTestCase, registerDiffTestCases} from '../support/register-diff-test-cases';

describe('diff-schemas anyOf', () => {
    const testCases: DiffTestCase[] = [
        {
            description: 'anyOf two "true" schemas to equivalent single "true" schema',
            examples: ['foo'],
            input: {
                a: {
                    anyOf: [
                        true,
                        true
                    ]
                },
                b: true
            },
            output: {
                added: false,
                removed: false
            }
        },
        {
            description: 'anyOf two "false" schemas to equivalent single "false" schema',
            examples: ['foo'],
            input: {
                a: {
                    anyOf: [
                        false,
                        false
                    ]
                },
                b: false
            },
            output: {
                added: false,
                removed: false
            }
        },
        {
            description: 'anyOf of "false" and "true" to equivalent anyOf "true" and "false"',
            examples: ['foo'],
            input: {
                a: {
                    anyOf: [
                        false,
                        true
                    ]
                },
                b: {
                    anyOf: [
                        true,
                        false
                    ]
                }
            },
            output: {
                added: false,
                removed: false
            }
        },
        {
            description: 'anyOf of "true" and single-type to equivalent anyOf single-type and "true"',
            examples: ['foo', 1],
            input: {
                a: {
                    anyOf: [
                        {type: 'string'},
                        true
                    ]
                },
                b: {
                    anyOf: [
                        true,
                        {type: 'string'}
                    ]
                }
            },
            output: {
                added: false,
                removed: false
            }
        },
        {
            description: 'anyOf of "false" and single-type to equivalent anyOf single-type and "false"',
            examples: ['foo', 1],
            input: {
                a: {
                    anyOf: [
                        {type: 'string'},
                        false
                    ]
                },
                b: {
                    anyOf: [
                        false,
                        {type: 'string'}
                    ]
                }
            },
            output: {
                added: false,
                removed: false
            }
        },
        {
            description: 'union of all types',
            examples: [[], true, 1, null, 1.1, {}, 'a'],
            input: {
                a: {
                    anyOf: [
                        {type: 'array'},
                        {type: 'boolean'},
                        {type: 'integer'},
                        {type: 'null'},
                        {type: 'number'},
                        {type: 'object'},
                        {type: 'string'}
                    ]
                },
                b: false
            },
            output: {
                added: false,
                removed: true
            }
        },
        {
            description: 'union of two disjoint sets using minItems and maxItems representing all arrays',
            examples: [[], [1], [1, 2], [1, 2, 3], [1, 2, 3, 4], 'foo'],
            input: {
                a: {
                    anyOf: [
                        {
                            minItems: 3,
                            type: 'array'
                        },
                        {
                            maxItems: 2,
                            type: 'array'
                        }
                    ]
                },
                b: {type: ['array']}
            },
            output: {
                added: false,
                removed: false
            }
        },
        {
            description: 'all arrays vs union of two disjoint sets using minItems and maxItems representing all arrays',
            examples: [[], [1], [1, 2], [1, 2, 3], [1, 2, 3, 4], 'foo'],
            input: {
                a: {
                    anyOf: [
                        {
                            minItems: 3,
                            type: 'array'
                        },
                        {
                            maxItems: 2,
                            type: 'array'
                        }
                    ]
                },
                b: {type: ['array']}
            },
            output: {
                added: false,
                removed: false
            }
        },
        {
            description: 'removing anyOf and replacing with items, resulting in a contradiction in the removed schema',
            examples: [[], [1], ['foo'], true, [1, 'foo'], [1, true], ['foo', true], [1, 'foo', true], 'foo'],
            input: {
                a: {
                    anyOf: [
                        {
                            items: {
                                type: 'string'
                            },
                            type: 'array'
                        },
                        {
                            items: {
                                type: 'number'
                            },
                            type: 'array'
                        }
                    ]
                },
                b: {
                    items: {
                        type: ['string', 'number']
                    },
                    type: 'array'
                }
            },
            output: {
                added: {
                    items: {
                        type: ['number', 'string']
                    },
                    not: {
                        anyOf: [
                            {
                                items: {
                                    type: 'number'
                                },
                                type: 'array'
                            },
                            {
                                items: {
                                    type: 'string'
                                },
                                type: 'array'
                            }
                        ]
                    },
                    type: 'array'
                },
                removed: false
            }
        },
        {
            description: 'any of not integers or integers to numbers',
            examples: [1, 1.5, 'foo'],
            input: {
                a: {
                    anyOf: [
                        {
                            not: {
                                type: 'integer'
                            },
                            type: 'number'
                        },
                        {
                            type: 'integer'
                        }
                    ]
                },
                b: {type: 'number'}
            },
            output: {
                added: false,
                removed: false
            }
        }
    ];

    registerDiffTestCases(testCases);
});
