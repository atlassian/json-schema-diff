import {DiffTestCase, registerDiffTestCases} from '../support/register-diff-test-cases';

describe('diff-schemas type string', () => {
    describe('minLength', () => {
        const testCases: DiffTestCase[] = [
            {
                description: 'string with minLength constraint removed',
                examples: ['', 'a', 'ab', 1],
                input: {
                    a: {
                        minLength: 1,
                        type: 'string'
                    },
                    b: false
                },
                output: {
                    added: false,
                    removed: {
                        minLength: 1,
                        type: 'string'
                    }
                }
            },
            {
                description: 'default min string length is set during complement operation',
                examples: ['', 'a', 'ab', 1],
                input: {
                    a: {
                        type: 'string'
                    },
                    b: {
                        minLength: 1,
                        type: 'string'
                    }
                },
                output: {
                    added: false,
                    removed: {
                        maxLength: 0,
                        type: 'string'
                    }
                }
            },
            {
                description: 'string with minLength 0 is all strings',
                examples: ['', 'a', 'ab', 1],
                input: {
                    a: {
                        minLength: 0,
                        type: 'string'
                    },
                    b: {
                        type: 'string'
                    }
                },
                output: {
                    added: false,
                    removed: false
                }
            },
            {
                description: 'string with minLength constraint has minLength made less restrictive',
                examples: ['', 'a', 'ab', 'abc', 'abcd', 'abcde', 1],
                input: {
                    a: {
                        minLength: 3,
                        type: 'string'
                    },
                    b: {
                        minLength: 1,
                        type: 'string'
                    }
                },
                output: {
                    added: {
                        maxLength: 2,
                        minLength: 1,
                        type: 'string'
                    },
                    removed: false
                }
            }
        ];
        registerDiffTestCases(testCases);
    });

    describe('maxLength', () => {
        const testCases: DiffTestCase[] = [
            {
                description: 'string with maxLength constraint removed',
                examples: ['', 'a', 'ab', 1],
                input: {
                    a: {
                        maxLength: 1,
                        type: 'string'
                    },
                    b: false
                },
                output: {
                    added: false,
                    removed: {
                        maxLength: 1,
                        type: 'string'
                    }
                }
            },
            {
                description: 'string with maxLength infinity is all strings',
                examples: ['', 'a', 'ab', 1],
                input: {
                    a: {
                        maxLength: Number.POSITIVE_INFINITY,
                        type: 'string'
                    },
                    b: {
                        type: 'string'
                    }
                },
                output: {
                    added: false,
                    removed: false
                }
            },
            {
                description: 'string with maxLength constraint has maxLength made more restrictive',
                examples: ['', 'a', 'ab', 'abc', 'abcd', 'abcde', 1],
                input: {
                    a: {
                        maxLength: 3,
                        type: 'string'
                    },
                    b: {
                        maxLength: 1,
                        type: 'string'
                    }
                },
                output: {
                    added: false,
                    removed: {
                        maxLength: 3,
                        minLength: 2,
                        type: 'string'
                    }
                }
            },
            {
                description: 'empty string not confused with no string',
                examples: ['', 'a', 'ab', 1],
                input: {
                    a: false,
                    b: {
                        maxLength: 0,
                        type: 'string'
                    }
                },
                output: {
                    added: {
                        maxLength: 0,
                        type: 'string'
                    },
                    removed: false
                }
            }
        ];
        registerDiffTestCases(testCases);
    });

    describe('maxLength and minLength', () => {
        const testCases: DiffTestCase[] = [
            {
                description: 'string with maxLength and minLength contradict each other',
                examples: ['', 'a', 'ab', 'abc', 'abcd', 'abcde', 1],
                input: {
                    a: {
                        maxLength: 1,
                        minLength: 3,
                        type: 'string'
                    },
                    b: false
                },
                output: {
                    added: false,
                    removed: false
                }
            },
            {
                description: 'string with maxLength Infinity and minLength 0 are all strings',
                examples: ['', 'a', 'ab', 'abc', 'abcd', 'abcde', 1],
                input: {
                    a: {
                        maxLength: Number.POSITIVE_INFINITY,
                        minLength: 0,
                        type: 'string'
                    },
                    b: {
                        type: 'string'
                    }
                },
                output: {
                    added: false,
                    removed: false
                }
            },
            {
                description: 'string length range is expanded in both directions',
                examples: ['', 'a', 'ab', 'abc', 'abcd', 'abcde', 'abcdef', 'abcdefg', 'abcdefgh', 1],
                input: {
                    a: {
                        maxLength: 5,
                        minLength: 3,
                        type: 'string'
                    },
                    b: {
                        maxLength: 7,
                        minLength: 1,
                        type: 'string'
                    }
                },
                output: {
                    added: {
                        anyOf: [
                            {
                                maxLength: 7,
                                minLength: 6,
                                type: 'string'
                            },
                            {
                                maxLength: 2,
                                minLength: 1,
                                type: 'string'
                            }
                        ]
                    },
                    removed: false
                }
            },
            {
                description: 'string length range is restricted in both directions',
                examples: ['', 'a', 'ab', 'abc', 'abcd', 'abcde', 'abcdef', 'abcdefg', 'abcdefgh', 1],
                input: {
                    a: {
                        maxLength: 7,
                        minLength: 1,
                        type: 'string'
                    },
                    b: {
                        maxLength: 5,
                        minLength: 3,
                        type: 'string'
                    }
                },
                output: {
                    added: false,
                    removed: {
                        anyOf: [
                            {
                                maxLength: 7,
                                minLength: 6,
                                type: 'string'
                            },
                            {
                                maxLength: 2,
                                minLength: 1,
                                type: 'string'
                            }
                        ]
                    }
                }
            },
            {
                description: 'string length range shifted so new range partially overlaps',
                examples: ['', 'a', 'ab', 'abc', 'abcd', 'abcde', 'abcdef', 'abcdefg', 'abcdefgh', 1],
                input: {
                    a: {
                        maxLength: 5,
                        minLength: 2,
                        type: 'string'
                    },
                    b: {
                        maxLength: 7,
                        minLength: 4,
                        type: 'string'
                    }
                },
                output: {
                    added: {
                        maxLength: 7,
                        minLength: 6,
                        type: 'string'
                    },
                    removed: {
                        maxLength: 3,
                        minLength: 2,
                        type: 'string'
                    }
                }
            },
            {
                description: 'string length range shifted so new range doesnt overlap',
                examples: ['', 'a', 'ab', 'abc', 'abcd', 'abcde', 'abcdef', 'abcdefg', 'abcdefgh', 1],
                input: {
                    a: {
                        maxLength: 3,
                        minLength: 1,
                        type: 'string'
                    },
                    b: {
                        maxLength: 7,
                        minLength: 5,
                        type: 'string'
                    }
                },
                output: {
                    added: {
                        maxLength: 7,
                        minLength: 5,
                        type: 'string'
                    },
                    removed: {
                        maxLength: 3,
                        minLength: 1,
                        type: 'string'
                    }
                }
            },
            {
                description: 'adding anyOf two string length ranges to existing range',
                examples: ['', 'a', 'ab', 'abc', 'abcd', 'abcde', 'abcdef', 'abcdefg', 'abcdefgh', 'abcdefghi', 1],
                input: {
                    a: {
                        maxLength: 7,
                        minLength: 2,
                        type: 'string'
                    },
                    b: {
                        anyOf: [
                            {
                                maxLength: 8,
                                minLength: 6,
                                type: 'string'
                            },
                            {
                                maxLength: 3,
                                minLength: 0,
                                type: 'string'
                            }
                        ]
                    }
                },
                output: {
                    added: {
                        anyOf: [
                            {
                                maxLength: 8,
                                minLength: 8,
                                type: 'string'
                            },
                            {
                                maxLength: 1,
                                type: 'string'
                            }
                        ]
                    },
                    removed: {
                        maxLength: 5,
                        minLength: 4,
                        type: 'string'
                    }
                }
            },
            {
                description: 'intersecting string min and max length in allOf are merged',
                examples: ['', 'a', 'ab', 'abc', 'abcd', 'abcde', 'abcdef', 'abcdefg', 'abcdefgh', 'abcdefghi', 1],
                input: {
                    a: false,
                    b: {
                        allOf: [
                            {
                                maxLength: 7,
                                minLength: 1,
                                type: 'string'
                            },
                            {
                                maxLength: 5,
                                minLength: 3,
                                type: 'string'
                            }
                        ]
                    }
                },
                output: {
                    added: {
                        maxLength: 5,
                        minLength: 3,
                        type: 'string'
                    },
                    removed: false
                }
            },
            {
                description: 'non overlapping string length ranges in allOf are ignored',
                examples: ['', 'a', 'ab', 'abc', 'abcd', 'abcde', 'abcdef', 'abcdefg', 'abcdefgh', 'abcdefghi', 1],
                input: {
                    a: false,
                    b: {
                        allOf: [
                            {
                                maxLength: 7,
                                minLength: 5,
                                type: 'string'
                            },
                            {
                                maxLength: 4,
                                minLength: 2,
                                type: 'string'
                            }
                        ]
                    }
                },
                output: {
                    added: false,
                    removed: false
                }
            },
            {
                description: 'adding oneOf string length ranges to existing range',
                examples: ['', 'a', 'ab', 'abc', 'abcd', 'abcde', 'abcdef', 'abcdefg', 'abcdefgh', 'abcdefghi', 1],
                input: {
                    a: {
                        maxLength: 7,
                        minLength: 2,
                        type: 'string'
                    },
                    b: {
                        oneOf: [
                            {
                                maxLength: 8,
                                minLength: 4,
                                type: 'string'
                            },
                            {
                                maxLength: 5,
                                minLength: 0,
                                type: 'string'
                            }
                        ]
                    }
                },
                output: {
                    added: {
                        anyOf: [
                            {
                                maxLength: 8,
                                minLength: 8,
                                type: 'string'
                            },
                            {
                                maxLength: 1,
                                type: 'string'
                            }
                        ]
                    },
                    removed: {
                        maxLength: 5,
                        minLength: 4,
                        type: 'string'
                    }
                }
            }
        ];
        registerDiffTestCases(testCases);
    });
});
