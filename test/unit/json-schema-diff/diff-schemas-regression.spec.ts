import {JsonSchema} from 'json-schema-spec-types';
import {invokeDiff} from '../support/invoke-diff';

describe('regression', () => {
    it('should not raise errors for a schema with property names matching Object.prototype properties', async () => {
        const schemaWithPropertyNamesFromPrototype: JsonSchema = {
            type: 'object',
            properties: {
                __defineGetter__: {type: 'string' as any},
                __defineSetter__: {type: 'string' as any},
                __lookupGetter__: {type: 'string' as any},
                __lookupSetter__: {type: 'string' as any},
                hasOwnProperty: {type: 'string' as any},
                isPrototypeOf: {type: 'string' as any},
                propertyIsEnumerable: {type: 'string' as any},
                toLocaleString: {type: 'string' as any},
                toSource: {type: 'string' as any},
                toString: {type: 'string' as any},
                valueOf: {type: 'string' as any}
            },
            additionalProperties: false
        };

        const diffResult = await invokeDiff(schemaWithPropertyNamesFromPrototype, schemaWithPropertyNamesFromPrototype);
        expect(diffResult.addedJsonSchema).toEqual(false);
        expect(diffResult.removedJsonSchema).toEqual(false);
    });
});
