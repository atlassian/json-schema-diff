import {DiffTestCase, registerDiffTestCases} from '../support/register-diff-test-cases';

describe('diff-schemas type number', () => {
    describe('integer', () => {
        const testCases: DiffTestCase[] = [
            {
                description: 'considers integer as a subset of number',
                examples: [1, 1.5, 'a'],
                input: {
                    a: {
                        type: ['number', 'integer']
                    },
                    b: {
                        type: 'number'
                    }
                },
                output: {
                    added: false,
                    removed: false
                }
            },
            {
                description: 'from integer to number',
                examples: [1, 1.5, 'a'],
                input: {
                    a: {
                        type: 'integer'
                    },
                    b: {
                        type: 'number'
                    }
                },
                output: {
                    added: {type: 'number', not: {type: 'integer'}},
                    removed: false
                }
            }
        ];

        registerDiffTestCases(testCases);
    });

    describe('maximum', () => {
        const testCases: DiffTestCase[] = [
            {
                description: 'numbers with maximum constraint to false',
                examples: [1.49, 1.5, 1.51, 'not-a-number'],
                input: {
                    a: {
                        type: 'number',
                        maximum: 1.5
                    },
                    b: false
                },
                output: {
                    added: false,
                    removed: {
                        type: 'number',
                        maximum: 1.5
                    }
                }
            },
            {
                description: 'all numbers to number with maximum constraint',
                examples: [1.99, 2, 2.01, 'not-a-number'],
                input: {
                    a: { type: 'number'},
                    b: { type: 'number', maximum: 2}
                },
                output: {
                    added: false,
                    removed: {
                        type: 'number',
                        exclusiveMinimum: 2
                    }
                }
            },
            {
                description: 'numbers with maximum constraint to more restrictive maximum constraint',
                examples: [-1, 2, 2.01, 4, 4.5, 'not-a-number'],
                input: {
                    a: { type: 'number', maximum: 4},
                    b: { type: 'number', maximum: 2}
                },
                output: {
                    added: false,
                    removed: {
                        type: 'number',
                        maximum: 4,
                        exclusiveMinimum: 2
                    }
                }
            }
        ];

        registerDiffTestCases(testCases);
    });

    describe('minimum', () => {
        const testCases: DiffTestCase[] = [
            {
                description: 'numbers with minimum constraint to false',
                examples: [-1, 1.5, 1.56, 'not-a-number'],
                input: {
                    a: {
                        type: 'number',
                        minimum: 1.5
                    },
                    b: false
                },
                output: {
                    added: false,
                    removed: {
                        type: 'number',
                        minimum: 1.5
                    }
                }
            },
            {
                description: 'all numbers to number with minimum constraint',
                examples: [-1, 1.99, 2, 2.01, 'not-a-number'],
                input: {
                    a: { type: 'number'},
                    b: { type: 'number', minimum: 2}
                },
                output: {
                    added: false,
                    removed: {
                        type: 'number',
                        exclusiveMaximum: 2
                    }
                }
            },
            {
                description: 'numbers with minimum constraint to more restrictive minimum constraint',
                examples: [-1, 2, 2.01, 4, 4.5, 'not-a-number'],
                input: {
                    a: { type: 'number', minimum: 2},
                    b: { type: 'number', minimum: 4}
                },
                output: {
                    added: false,
                    removed: {
                        type: 'number',
                        minimum: 2,
                        exclusiveMaximum: 4
                    }
                }
            }
        ];

        registerDiffTestCases(testCases);
    });

    describe('exclusiveMaximum', () => {
        const testCases: DiffTestCase[] = [
            {
                description: 'numbers with exclusiveMaximum constraint to false',
                examples: [1.49, 1.5, 1.501, 'not-a-number'],
                input: {
                    a: {
                        type: 'number',
                        exclusiveMaximum: 1.5
                    },
                    b: false
                },
                output: {
                    added: false,
                    removed: {
                        type: 'number',
                        exclusiveMaximum: 1.5
                    }
                }
            },
            {
                description: 'all numbers to number with exclusiveMaximum constraint',
                examples: [-1, 1.99, 2, 2.01, 'not-a-number'],
                input: {
                    a: { type: 'number'},
                    b: { type: 'number', exclusiveMaximum: 2}
                },
                output: {
                    added: false,
                    removed: {
                        type: 'number',
                        minimum: 2
                    }
                }
            },
            {
                description: 'numbers with exclusiveMaximum constraint to more restrictive exclusiveMaximum constraint',
                examples: [-1, 2, 2.01, 4, 4.5, 'not-a-number'],
                input: {
                    a: { type: 'number', exclusiveMaximum: 4},
                    b: { type: 'number', exclusiveMaximum: 2}
                },
                output: {
                    added: false,
                    removed: {
                        type: 'number',
                        minimum: 2,
                        exclusiveMaximum: 4
                    }
                }
            }
        ];

        registerDiffTestCases(testCases);
    });

    describe('exclusiveMinimum', () => {
        const testCases: DiffTestCase[] = [
            {
                description: 'numbers with exclusiveMinimum constraint to false',
                examples: [1.49, 1.5, 1.501, 'not-a-number'],
                input: {
                    a: {
                        type: 'number',
                        exclusiveMinimum: 1.5
                    },
                    b: false
                },
                output: {
                    added: false,
                    removed: {
                        type: 'number',
                        exclusiveMinimum: 1.5
                    }
                }
            },
            {
                description: 'all numbers to number with exclusiveMinimum constraint',
                examples: [-1, 1.99, 2, 2.01, 'not-a-number'],
                input: {
                    a: { type: 'number'},
                    b: { type: 'number', exclusiveMinimum: 2}
                },
                output: {
                    added: false,
                    removed: {
                        type: 'number',
                        maximum: 2
                    }
                }
            },
            {
                description: 'numbers with exclusiveMinimum constraint to more restrictive exclusiveMinimum constraint',
                examples: [1.99, 2, 2.01, 3.99, 4, 4.01, 'not-a-number'],
                input: {
                    a: { type: 'number', exclusiveMinimum: 2},
                    b: { type: 'number', exclusiveMinimum: 4}
                },
                output: {
                    added: false,
                    removed: {
                        type: 'number',
                        maximum: 4,
                        exclusiveMinimum: 2
                    }
                }
            }
        ];

        registerDiffTestCases(testCases);
    });

    describe('minimum and maximum', () => {
        const testCases: DiffTestCase[] = [
            {
                description: 'minimum equal than maximum not a contradiction since both are inclusive',
                examples: [1, 2, 3],
                input: {
                    a: {type: 'number', minimum: 2, maximum: 2},
                    b: false
                },
                output: {
                    added: false,
                    removed: {type: 'number', minimum: 2, maximum: 2}
                }
            }
        ];

        registerDiffTestCases(testCases);
    });

    describe('minimum and exclusiveMaximum', () => {
        const testCases: DiffTestCase[] = [
            {
                description: 'minimum and exclusiveMaximum contradiction',
                examples: [1, 'not-a-number'],
                input: {
                    a: {type: 'number', minimum: 4, exclusiveMaximum: 2},
                    b: false
                },
                output: {
                    added: false,
                    removed: false
                }
            },
            {
                description: 'all numbers to numbers with minimum and exclusiveMaximum constraints',
                examples: [-1.01, -1, 1, 2, 2.01, 'not-a-number'],
                input: {
                    a: {type: 'number'},
                    b: {type: 'number', minimum: -1, exclusiveMaximum: 2}
                },
                output: {
                    added: false,
                    removed: {
                        anyOf: [
                            {type: 'number', minimum: 2},
                            {type: 'number', exclusiveMaximum: -1}
                        ]
                    }
                }
            }
        ];

        registerDiffTestCases(testCases);
    });

    describe('minimum and exclusiveMinimum', () => {
        const testCases: DiffTestCase[] = [
            {
                description: 'simplified to minimum when minimum is greater than exclusiveMinimum',
                examples: [0, 1, 2, 3, 4],
                input: {
                    a: {type: 'number', minimum: 3, exclusiveMinimum: 1},
                    b: false
                },
                output: {
                    added: false,
                    removed: {type: 'number', minimum: 3}
                }
            },
            {
                description: 'simplified to exclusiveMinimum when exclusiveMinimum is greater than minimum',
                examples: [0, 1, 2, 3, 4],
                input: {
                    a: {type: 'number', minimum: 1, exclusiveMinimum: 3},
                    b: false
                },
                output: {
                    added: false,
                    removed: {type: 'number', exclusiveMinimum: 3}
                }
            },
            {
                description: 'simplified to just exclusiveMinimum when exclusiveMinimum and minimum are equal',
                examples: [2, 3, 4],
                input: {
                    a: {type: 'number', minimum: 3, exclusiveMinimum: 3},
                    b: false
                },
                output: {
                    added: false,
                    removed: {type: 'number', exclusiveMinimum: 3}
                }
            }
        ];

        registerDiffTestCases(testCases);
    });

    describe('maximum and exclusiveMinimum', () => {
        const testCases: DiffTestCase[] = [
            {
                description: 'maximum and exclusiveMinimum contradiction',
                examples: [1, 'not-a-number'],
                input: {
                    a: {type: 'number', exclusiveMinimum: 4, maximum: 2},
                    b: false
                },
                output: {
                    added: false,
                    removed: false
                }
            },
            {
                description: 'all numbers to numbers with maximum and exclusiveMinimum constraints',
                examples: [-1.01, -1, 1.1, 1.99, 2, 2.01, 'not-a-number'],
                input: {
                    a: {type: 'number'},
                    b: {type: 'number', exclusiveMinimum: -1, maximum: 2}
                },
                output: {
                    added: false,
                    removed: {
                        anyOf: [
                            {type: 'number', maximum: -1},
                            {type: 'number', exclusiveMinimum: 2}
                        ]
                    }
                }
            }
        ];

        registerDiffTestCases(testCases);
    });

    describe('maximum and exclusiveMaximum', () => {
        const testCases: DiffTestCase[] = [
            {
                description: 'simplified to maximum when maximum is lower than exclusiveMaximum',
                examples: [0, 1, 2, 3, 4],
                input: {
                    a: {type: 'number', maximum: 1, exclusiveMaximum: 3},
                    b: false
                },
                output: {
                    added: false,
                    removed: {type: 'number', maximum: 1}
                }
            },
            {
                description: 'simplified to exclusiveMaximum when exclusiveMaximum is lower than maximum',
                examples: [0, 1, 2, 3, 4],
                input: {
                    a: {type: 'number', maximum: 3, exclusiveMaximum: 1},
                    b: false
                },
                output: {
                    added: false,
                    removed: {type: 'number', exclusiveMaximum: 1}
                }
            },
            {
                description: 'simplified to just exclusiveMaximum when exclusiveMaximum and maximum are equal',
                examples: [2, 3, 4],
                input: {
                    a: {type: 'number', maximum: 3, exclusiveMaximum: 3},
                    b: false
                },
                output: {
                    added: false,
                    removed: {type: 'number', exclusiveMaximum: 3}
                }
            }
        ];

        registerDiffTestCases(testCases);
    });

    describe('exclusiveMinimum and exclusiveMaximum', () => {
        const testCases: DiffTestCase[] = [
            {
                description: 'exclusiveMinimum greater than exclusiveMaximum contradiction',
                examples: [1, 2, 3, 4],
                input: {
                    a: {type: 'number', exclusiveMinimum: 3, exclusiveMaximum: 2},
                    b: false
                },
                output: {
                    added: false,
                    removed: false
                }
            },
            {
                description: 'exclusiveMinimum equal than exclusiveMaximum contradiction',
                examples: [1, 2, 3],
                input: {
                    a: {type: 'number', exclusiveMinimum: 2, exclusiveMaximum: 2},
                    b: false
                },
                output: {
                    added: false,
                    removed: false
                }
            }
        ];

        registerDiffTestCases(testCases);
    });

    describe('integer and ranges', () => {
        const testCases: DiffTestCase[] = [
            {
                description: 'contradiction when no integer exists in minimum/maximum range',
                examples: [1, 1.2, 1.3, 1.8, 1.9, 2],
                input: {
                    a: {type: 'integer', minimum: 1.3, maximum: 1.8},
                    b: false
                },
                output: {
                    added: false,
                    removed: false
                }
            },
            {
                description: 'contradiction when no integer exists in exclusiveMinimum/exclusiveMaximum range',
                examples: [0, 1, 1.5, 2, 3],
                input: {
                    a: {type: 'integer', exclusiveMinimum: 1, exclusiveMaximum: 2},
                    b: false
                },
                output: {
                    added: false,
                    removed: false
                }
            },
            {
                description: 'integer with decimal minimum simplified by applying ceil function',
                examples: [0, 0.79, 0.8, 0.81, 1],
                input: {
                    a: {type: 'integer', minimum: 0.8},
                    b: false
                },
                output: {
                    added: false,
                    removed: {type: 'integer', minimum: 1}
                }
            },
            {
                description: 'integer with decimal exclusiveMinimum simplified by applying floor function',
                examples: [0, 1, 1.79, 1.8, 1.81, 1, 2],
                input: {
                    a: {type: 'integer', exclusiveMinimum: 1.8},
                    b: false
                },
                output: {
                    added: false,
                    removed: {type: 'integer', exclusiveMinimum: 1}
                }
            },
            {
                description: 'integer with decimal maximum simplified by applying floor function',
                examples: [0, 1, 1.8, 1.81, 2],
                input: {
                    a: {type: 'integer', maximum: 1.8},
                    b: false
                },
                output: {
                    added: false,
                    removed: {type: 'integer', maximum: 1}
                }
            },
            {
                description: 'integer with decimal exclusiveMaximum simplified by applying ceil function',
                examples: [1, 1.79, 1.8, 1.81, 2],
                input: {
                    a: {type: 'integer', exclusiveMaximum: 1.8},
                    b: false
                },
                output: {
                    added: false,
                    removed: {type: 'integer', exclusiveMaximum: 2}
                }
            },
            {
                description: 'number with minimum and maximum constraints to integer',
                examples: [-6, -5.5, -5, 4, 4.5, 5, 5.5, 6,  'not-a-number'],
                input: {
                    a: {type: 'number', minimum: -5, maximum: 5},
                    b: {type: 'integer'}
                },
                output:  {
                    added: {
                        anyOf: [
                            {type: 'integer', exclusiveMinimum: 5},
                            {type: 'integer', exclusiveMaximum: -5}
                        ]
                    },
                    removed: {type: 'number', minimum: -5, maximum: 5, not: {type: 'integer'}
                    }
                }
            },
            {
                description: 'number with min/max constraint to integer with partially overlapping min/max constraint',
                examples: [-6, -5.1, -5, 0.5, 1, 3, 5, 5.1, 6, 7, 7.1, 8],
                input: {
                    a: {type: 'number', minimum: -5, maximum: 5},
                    b: {type: 'integer', minimum: 1, maximum: 7}
                },
                output:  {
                    added: {type: 'integer', exclusiveMinimum: 5, maximum: 7},
                    removed: {
                        anyOf: [
                            {type: 'number', minimum: -5, exclusiveMaximum: 1 },
                            {type: 'number', minimum: -5, maximum: 5, not: {type: 'integer'}}
                        ]
                    }
                }
            }
        ];

        registerDiffTestCases(testCases);
    });
});
