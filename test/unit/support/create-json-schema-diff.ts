import {JsonSchemaDiff} from '../../../lib/json-schema-diff';
import {FileReader} from '../../../lib/json-schema-diff/file-reader';
import {createMockFileSystem, MockFileSystem} from './mocks/mock-file-system';
import {createMockReporter, MockReporter} from './mocks/mock-reporter';
import {loadConfig} from '../../../lib/json-schema-diff/config';

interface CreateJsonSchemaDiffMocks {
    mockFileSystem: MockFileSystem;
    mockReporter: MockReporter;
}

export const createJsonSchemaDiffWithMocks = (mocks: CreateJsonSchemaDiffMocks): JsonSchemaDiff => {
    const fileReader = new FileReader(mocks.mockFileSystem);
    const mockEnvironment = {};
    const config = loadConfig(mockEnvironment);
    return new JsonSchemaDiff(fileReader, mocks.mockReporter, config);
};

export const createJsonSchemaDiff = (): JsonSchemaDiff => {
    return createJsonSchemaDiffWithMocks({
        mockFileSystem: createMockFileSystem(),
        mockReporter: createMockReporter()
    });
};
