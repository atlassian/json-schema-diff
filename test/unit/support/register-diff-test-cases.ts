import Ajv from 'ajv';
import {JsonSchema} from 'json-schema-spec-types';
import {invokeDiff} from './invoke-diff';

export interface DiffTestCase {
    description: string;
    examples: any[];
    focus?: boolean;
    input: {
        a: JsonSchema;
        b: JsonSchema;
    };
    output: {
        added: JsonSchema;
        removed: JsonSchema;
    };
}

export const registerDiffTestCases = (testCases: DiffTestCase[]) => {
    testCases.forEach((testCase) => {
        const describeFn = (testCase.focus) ? fdescribe : describe;
        describeFn(testCase.description, () => {
            it('(source = A, destination = B)', async () => {
                const diffResult = await invokeDiff(testCase.input.a, testCase.input.b);

                expect(diffResult.addedJsonSchema).toEqual(testCase.output.added);
                expect(diffResult.removedJsonSchema).toEqual(testCase.output.removed);
            });

            it('(source = B, destination = A)', async () => {
                const diffResult = await invokeDiff(testCase.input.b, testCase.input.a);

                expect(diffResult.addedJsonSchema).toEqual(testCase.output.removed);
                expect(diffResult.removedJsonSchema).toEqual(testCase.output.added);
            });

            const ajv = new Ajv({strict: false});
            const validateA = ajv.compile(testCase.input.a);
            const validateB = ajv.compile(testCase.input.b);
            const validateAdded = ajv.compile(testCase.output.added);
            const validateRemoved = ajv.compile(testCase.output.removed);

            testCase.examples.forEach((example) => {
                const valueAsString = JSON.stringify(example);
                it(`example: ${valueAsString}`, () => {
                    const acceptedByA = validateA(example);
                    const acceptedByB = validateB(example);

                    const acceptedByAdded = !acceptedByA && acceptedByB;
                    const acceptedByRemoved = acceptedByA && !acceptedByB;

                    expect(validateAdded(example)).toBe(
                        acceptedByAdded,
                        'when validating example against the added schema the result was inconsistent'
                    );
                    expect(validateRemoved(example)).toBe(
                        acceptedByRemoved,
                        'when validating example against the removed schema the result was inconsistent'
                    );
                });
            });
        });
    });
};
