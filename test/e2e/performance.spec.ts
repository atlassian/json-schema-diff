import {invokeJsonSchemaDiff} from '../support/invoke-json-schema-diff';
import {expectToFail} from '../support/expect-to-fail';

const TEN_SECONDS_TIMEOUT = 10_000;

describe('performance scenarios', () => {
    it('executes quickly on schemas with CPU intensive calculations by disabling the absorption law simplification', async () => {
        await expectToFail(invokeJsonSchemaDiff(
            './test/e2e/fixtures/cpu-intensive-source.json',
            './test/e2e/fixtures/cpu-intensive-destination.json',
            {
                JSON_SCHEMA_DIFF_APPLY_ABSORPTION_IN_CARTESIAN_PRODUCT: 'false'
            }
        ));
    }, TEN_SECONDS_TIMEOUT);

    it('executes quickly on schemas with memory intensive calculations by enabling the absorption law simplification', async () => {
        await invokeJsonSchemaDiff(
            './test/e2e/fixtures/memory-intensive-schema.json',
            './test/e2e/fixtures/memory-intensive-schema.json',
            {
                JSON_SCHEMA_DIFF_APPLY_ABSORPTION_IN_CARTESIAN_PRODUCT: 'true'
            }
        );
    }, TEN_SECONDS_TIMEOUT);
});