import {expectToFail} from '../support/expect-to-fail';
import {invokeJsonSchemaDiff} from '../support/invoke-json-schema-diff';

describe('json-schema-diff cli', () => {
    it('should find a difference in type', async () => {
        const error = await expectToFail(
            invokeJsonSchemaDiff(
                './test/e2e/fixtures/type-string-spec.json',
                './test/e2e/fixtures/type-number-spec.json'
            )
        );

        expect(error.message).toContain('Breaking changes found between the two schemas');
        expect(error.message).toContain('added');
        expect(error.message).toContain('removed');
    });

    it('should be successful when no differences are found', async () => {
        const output = await invokeJsonSchemaDiff(
            './test/e2e/fixtures/type-string-spec.json',
            './test/e2e/fixtures/type-string-spec.json'
        );

        expect(output).toContain('No differences found');
    });

    it('should return the error when a file cannot be found', async () => {
        const error = await expectToFail(
            invokeJsonSchemaDiff(
                './test/e2e/fixtures/type-string-spec.json',
                './test/e2e/fixtures/missing-file.json'
            )
        );

        expect(error.message).toContain('Error loading "./test/e2e/fixtures/missing-file.json"');
    });
});
