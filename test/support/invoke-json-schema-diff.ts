import {exec} from 'child_process';

export const invokeJsonSchemaDiff = (
    sourceSchemaFile: string,
    destinationSchemaFile: string,
    env?: NodeJS.ProcessEnv
): Promise<string> => {
    return new Promise((resolve, reject) => {
        const processEnv = {...process.env, ...env};
        exec(`./bin/json-schema-diff-local ${sourceSchemaFile} ${destinationSchemaFile}`, {env: processEnv}, (error, stdout, stderr) => {
            const output = stdout.toString() + stderr.toString();

            if (error) {
                reject(new Error(`${error.message} ${output}`));
            } else {
                resolve(output);
            }
        });
    });
};