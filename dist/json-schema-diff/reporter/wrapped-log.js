"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.WrappedLog = void 0;
class WrappedLog {
    info(message) {
        console.log(message);
    }
    error(error) {
        console.error(error);
    }
}
exports.WrappedLog = WrappedLog;
