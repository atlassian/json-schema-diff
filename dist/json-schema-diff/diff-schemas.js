"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.diffSchemas = void 0;
const logger_1 = require("./common/logger");
const dereference_schema_1 = require("./diff-schemas/dereference-schema");
const parse_as_json_set_1 = require("./diff-schemas/parse-as-json-set");
const validate_schemas_1 = require("./diff-schemas/validate-schemas");
const diffSchemas = (sourceSchema, destinationSchema, context) => __awaiter(void 0, void 0, void 0, function* () {
    const [dereferencedSourceSchema, dereferencedDestinationSchema] = yield Promise.all([
        (0, dereference_schema_1.dereferenceSchema)(sourceSchema), (0, dereference_schema_1.dereferenceSchema)(destinationSchema)
    ]);
    yield (0, validate_schemas_1.validateSchemas)(sourceSchema, destinationSchema);
    const sourceSet = (0, parse_as_json_set_1.parseAsJsonSet)(dereferencedSourceSchema, context);
    (0, logger_1.logSetDebug)('sourceSet', sourceSet);
    const destinationSet = (0, parse_as_json_set_1.parseAsJsonSet)(dereferencedDestinationSchema, context);
    (0, logger_1.logSetDebug)('destinationSet', destinationSet);
    const intersectionOfSets = sourceSet.intersect(destinationSet);
    (0, logger_1.logSetDebug)('intersectionOfSets', intersectionOfSets);
    const intersectionOfSetsComplement = intersectionOfSets.complement();
    (0, logger_1.logSetDebug)('intersectionOfSetsComplement', intersectionOfSetsComplement);
    const addedToDestinationSet = intersectionOfSetsComplement.intersect(destinationSet);
    (0, logger_1.logSetDebug)('addedToDestinationSet', addedToDestinationSet);
    const removedFromDestinationSet = intersectionOfSetsComplement.intersect(sourceSet);
    (0, logger_1.logSetDebug)('removedFromDestinationSet', removedFromDestinationSet);
    return {
        addedJsonSchema: addedToDestinationSet.toJsonSchema(),
        additionsFound: addedToDestinationSet.type !== 'empty',
        removalsFound: removedFromDestinationSet.type !== 'empty',
        removedJsonSchema: removedFromDestinationSet.toJsonSchema()
    };
});
exports.diffSchemas = diffSchemas;
