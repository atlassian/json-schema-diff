"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.parseAsJsonSet = void 0;
const create_json_set_1 = require("./set-factories/create-json-set");
const keyword_defaults_1 = require("./set/keyword-defaults");
class JsonToSetParser {
    constructor(context) {
        this.context = context;
    }
    parse(schema) {
        return this.parseSchemaOrUndefinedAsJsonSet(schema);
    }
    parseSchemaOrUndefinedAsJsonSet(schema) {
        return (typeof schema === 'boolean' || schema === undefined)
            ? this.parseBooleanSchema(schema)
            : this.parseCoreSchemaMetaSchema(schema);
    }
    parseBooleanSchema(schema) {
        const allowsAllJsonValues = schema === undefined ? true : schema;
        return allowsAllJsonValues ? (0, create_json_set_1.createAllJsonSet)() : (0, create_json_set_1.createEmptyJsonSet)();
    }
    ;
    parseCoreSchemaMetaSchema(schema) {
        const typeKeywordsSet = this.parseTypeKeywords(schema);
        const booleanLogicKeywordSets = this.parseBooleanLogicKeywords(schema);
        return booleanLogicKeywordSets.reduce((accumulator, set) => accumulator.intersect(set), typeKeywordsSet);
    }
    ;
    parseTypeKeywords(schema) {
        return (0, create_json_set_1.createJsonSetFromParsedSchemaKeywords)({
            additionalProperties: this.parseSchemaOrUndefinedAsJsonSet(schema.additionalProperties),
            exclusiveMaximum: this.parseNumericKeyword(schema.exclusiveMaximum, keyword_defaults_1.defaultMaximum),
            exclusiveMinimum: this.parseNumericKeyword(schema.exclusiveMinimum, keyword_defaults_1.defaultMinimum),
            items: this.parseSchemaOrUndefinedAsJsonSet(schema.items),
            maximum: this.parseNumericKeyword(schema.maximum, keyword_defaults_1.defaultMaximum),
            maxItems: this.parseNumericKeyword(schema.maxItems, keyword_defaults_1.defaultMaxItems),
            maxLength: this.parseNumericKeyword(schema.maxLength, keyword_defaults_1.defaultMaxLength),
            maxProperties: this.parseNumericKeyword(schema.maxProperties, keyword_defaults_1.defaultMaxProperties),
            minimum: this.parseNumericKeyword(schema.minimum, keyword_defaults_1.defaultMinimum),
            minItems: this.parseNumericKeyword(schema.minItems, keyword_defaults_1.defaultMinItems),
            minLength: this.parseNumericKeyword(schema.minLength, keyword_defaults_1.defaultMinLength),
            minProperties: this.parseNumericKeyword(schema.minProperties, keyword_defaults_1.defaultMinProperties),
            properties: this.parseSchemaProperties(schema.properties),
            required: this.parseRequiredKeyword(schema.required),
            type: this.parseType(schema.type)
        }, this.context);
    }
    parseBooleanLogicKeywords(schema) {
        return [
            ...this.parseAnyOfKeyword(schema.anyOf),
            ...this.parseAllOfKeyword(schema.allOf),
            ...this.parseNotKeyword(schema.not),
            ...this.parseOneOfKeyword(schema.oneOf)
        ];
    }
    parseAnyOfKeyword(anyOfSchemas) {
        if (!anyOfSchemas) {
            return [];
        }
        const parsedAnyOfSet = anyOfSchemas
            .map((schema) => this.parseSchemaOrUndefinedAsJsonSet(schema))
            .reduce((accumulator, set) => accumulator.union(set));
        return [parsedAnyOfSet];
    }
    ;
    parseAllOfKeyword(allOfSchemas) {
        return (allOfSchemas || []).map((schema) => this.parseSchemaOrUndefinedAsJsonSet(schema));
    }
    parseNotKeyword(notSchema) {
        if (!notSchema) {
            return [];
        }
        return [this.parseSchemaOrUndefinedAsJsonSet(notSchema).complement()];
    }
    ;
    parseOneOfKeyword(oneOfSchemas) {
        if (!oneOfSchemas) {
            return [];
        }
        // oneOf: [A, B, C] = (A && !B && !C) || (!A && B && !C) || (!A && !B && C)
        const parsedSchemaList = oneOfSchemas.map((schema) => this.parseSchemaOrUndefinedAsJsonSet(schema));
        const complementedSchemas = parsedSchemaList.map((schema) => schema.complement());
        const parsedOneOfSchema = parsedSchemaList
            .map((schema, i) => [...complementedSchemas.slice(0, i), schema, ...complementedSchemas.slice(i + 1)])
            .map((schemaGroup) => schemaGroup.reduce((acc, schema) => acc.intersect(schema)))
            .reduce((acc, schemaGroup) => acc.union(schemaGroup));
        return [parsedOneOfSchema];
    }
    parseSchemaProperties(schemaProperties = {}) {
        const objectSetProperties = {};
        for (const propertyName of Object.keys(schemaProperties)) {
            const propertySchema = schemaProperties[propertyName];
            objectSetProperties[propertyName] = this.parseSchemaOrUndefinedAsJsonSet(propertySchema);
        }
        return objectSetProperties;
    }
    ;
    parseNumericKeyword(keywordValue, defaultValue) {
        return typeof keywordValue === 'number' ? keywordValue : defaultValue;
    }
    parseRequiredKeyword(required) {
        return required || keyword_defaults_1.defaultRequired;
    }
    parseType(type) {
        if (!type) {
            return keyword_defaults_1.defaultTypes;
        }
        return typeof type === 'string' ? [type] : type;
    }
    ;
}
const parseAsJsonSet = (schema, context) => new JsonToSetParser(context).parse(schema);
exports.parseAsJsonSet = parseAsJsonSet;
