"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.createStringSet = void 0;
const keyword_defaults_1 = require("../set/keyword-defaults");
const string_subset_1 = require("../set/subset/string-subset");
const type_set_1 = require("../set/type-set");
const is_type_supported_1 = require("./is-type-supported");
const supportsAllStrings = (stringSetParsedKeywords) => stringSetParsedKeywords.maxLength === keyword_defaults_1.defaultMaxLength
    && stringSetParsedKeywords.minLength === keyword_defaults_1.defaultMinLength;
const createStringSubset = (stringSetParsedKeywords) => {
    if (!(0, is_type_supported_1.isTypeSupported)(stringSetParsedKeywords.type, 'string')) {
        return string_subset_1.emptyStringSubset;
    }
    if (supportsAllStrings(stringSetParsedKeywords)) {
        return string_subset_1.allStringSubset;
    }
    return (0, string_subset_1.createStringSubsetFromConfig)(Object.assign({}, stringSetParsedKeywords));
};
const createStringSet = (stringSetParsedKeywords, context) => {
    const stringSubset = createStringSubset(stringSetParsedKeywords);
    return (0, type_set_1.createTypeSetFromSubsets)('string', [stringSubset], context);
};
exports.createStringSet = createStringSet;
