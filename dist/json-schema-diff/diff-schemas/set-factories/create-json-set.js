"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.createEmptyJsonSet = exports.createAllJsonSet = exports.createJsonSetFromParsedSchemaKeywords = void 0;
const json_set_1 = require("../set/json-set");
const create_array_set_1 = require("./create-array-set");
const create_number_set_1 = require("./create-number-set");
const create_object_set_1 = require("./create-object-set");
const create_string_set_1 = require("./create-string-set");
const create_type_set_1 = require("./create-type-set");
const createJsonSetFromParsedSchemaKeywords = (parsedSchemaKeywords, context) => (0, json_set_1.createJsonSetFromConfig)({
    array: (0, create_array_set_1.createArraySet)(parsedSchemaKeywords, context),
    boolean: (0, create_type_set_1.createTypeSet)('boolean', parsedSchemaKeywords.type, context),
    null: (0, create_type_set_1.createTypeSet)('null', parsedSchemaKeywords.type, context),
    number: (0, create_number_set_1.createNumberSet)(parsedSchemaKeywords, context),
    object: (0, create_object_set_1.createObjectSet)(parsedSchemaKeywords, context),
    string: (0, create_string_set_1.createStringSet)(parsedSchemaKeywords, context)
});
exports.createJsonSetFromParsedSchemaKeywords = createJsonSetFromParsedSchemaKeywords;
const createAllJsonSet = () => json_set_1.allJsonSet;
exports.createAllJsonSet = createAllJsonSet;
const createEmptyJsonSet = () => json_set_1.emptyJsonSet;
exports.createEmptyJsonSet = createEmptyJsonSet;
