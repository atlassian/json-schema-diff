"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.createArraySet = void 0;
const keyword_defaults_1 = require("../set/keyword-defaults");
const array_subset_1 = require("../set/subset/array-subset");
const type_set_1 = require("../set/type-set");
const is_type_supported_1 = require("./is-type-supported");
const supportsAllArrays = (arraySetParsedKeywords) => arraySetParsedKeywords.items.type === 'all'
    && arraySetParsedKeywords.minItems === keyword_defaults_1.defaultMinItems
    && arraySetParsedKeywords.maxItems === keyword_defaults_1.defaultMaxItems;
const createArraySubset = (arraySetParsedKeywords) => {
    if (!(0, is_type_supported_1.isTypeSupported)(arraySetParsedKeywords.type, 'array')) {
        return array_subset_1.emptyArraySubset;
    }
    if (supportsAllArrays(arraySetParsedKeywords)) {
        return array_subset_1.allArraySubset;
    }
    return (0, array_subset_1.createArraySubsetFromConfig)(Object.assign(Object.assign({}, arraySetParsedKeywords), { not: [] }));
};
const createArraySet = (arraySetParsedKeywords, context) => {
    const arraySubset = createArraySubset(arraySetParsedKeywords);
    return (0, type_set_1.createTypeSetFromSubsets)('array', [arraySubset], context);
};
exports.createArraySet = createArraySet;
