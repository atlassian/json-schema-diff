"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.isTypeSupported = void 0;
const isTypeSupported = (parsedTypeKeyword, type) => parsedTypeKeyword.indexOf(type) >= 0;
exports.isTypeSupported = isTypeSupported;
