"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.createObjectSet = void 0;
const _ = require("lodash");
const keyword_defaults_1 = require("../set/keyword-defaults");
const object_subset_1 = require("../set/subset/object-subset");
const type_set_1 = require("../set/type-set");
const is_type_supported_1 = require("./is-type-supported");
/* eslint-disable-next-line complexity */
const supportsAllObjects = (objectSetParsedKeywords) => {
    const everyPropertyIsAll = Object.values(objectSetParsedKeywords.properties)
        .every((propertySet) => propertySet.type === 'all');
    return everyPropertyIsAll
        && _.isEqual(objectSetParsedKeywords.required, keyword_defaults_1.defaultRequired)
        && objectSetParsedKeywords.additionalProperties.type === 'all'
        && objectSetParsedKeywords.minProperties === keyword_defaults_1.defaultMinProperties
        && objectSetParsedKeywords.maxProperties === keyword_defaults_1.defaultMaxProperties;
};
const createObjectSubset = (objectSetParsedKeywords) => {
    if (!(0, is_type_supported_1.isTypeSupported)(objectSetParsedKeywords.type, 'object')) {
        return object_subset_1.emptyObjectSubset;
    }
    if (supportsAllObjects(objectSetParsedKeywords)) {
        return object_subset_1.allObjectSubset;
    }
    return (0, object_subset_1.createObjectSubsetFromConfig)(Object.assign(Object.assign({}, objectSetParsedKeywords), { not: [] }));
};
const createObjectSet = (objectSetParsedKeywords, context) => {
    const objectSubset = createObjectSubset(objectSetParsedKeywords);
    return (0, type_set_1.createTypeSetFromSubsets)('object', [objectSubset], context);
};
exports.createObjectSet = createObjectSet;
