"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.createTypeSet = void 0;
const subset_1 = require("../set/subset/subset");
const type_set_1 = require("../set/type-set");
const is_type_supported_1 = require("./is-type-supported");
const createTypeSubset = (setType, types) => (0, is_type_supported_1.isTypeSupported)(types, setType) ? new subset_1.AllSubset(setType) : new subset_1.EmptySubset(setType);
const createTypeSet = (setType, types, context) => (0, type_set_1.createTypeSetFromSubsets)(setType, [createTypeSubset(setType, types)], context);
exports.createTypeSet = createTypeSet;
