"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.createNumberSet = void 0;
const keyword_defaults_1 = require("../set/keyword-defaults");
const number_subset_1 = require("../set/subset/number-subset");
const type_set_1 = require("../set/type-set");
const is_type_supported_1 = require("./is-type-supported");
// eslint-disable-next-line complexity
const supportsAllNumbers = (numberSetParsedKeywords) => (0, is_type_supported_1.isTypeSupported)(numberSetParsedKeywords.type, 'number')
    && numberSetParsedKeywords.maximum === keyword_defaults_1.defaultMaximum
    && numberSetParsedKeywords.minimum === keyword_defaults_1.defaultMinimum
    && numberSetParsedKeywords.exclusiveMaximum === keyword_defaults_1.defaultMaximum
    && numberSetParsedKeywords.exclusiveMinimum === keyword_defaults_1.defaultMinimum;
const supportsNoNumbers = (numberSetParsedKeywords) => !(0, is_type_supported_1.isTypeSupported)(numberSetParsedKeywords.type, 'number') && !(0, is_type_supported_1.isTypeSupported)(numberSetParsedKeywords.type, 'integer');
const createNumberSubset = (numberSetParsedKeywords) => {
    if (supportsNoNumbers(numberSetParsedKeywords)) {
        return number_subset_1.emptyNumberSubset;
    }
    if (supportsAllNumbers(numberSetParsedKeywords)) {
        return number_subset_1.allNumberSubset;
    }
    const isInteger = !(0, is_type_supported_1.isTypeSupported)(numberSetParsedKeywords.type, 'number');
    return (0, number_subset_1.createNumberSubsetFromConfig)({
        exclusiveMaximum: numberSetParsedKeywords.exclusiveMaximum,
        exclusiveMinimum: numberSetParsedKeywords.exclusiveMinimum,
        maximum: numberSetParsedKeywords.maximum,
        minimum: numberSetParsedKeywords.minimum,
        isInteger,
        not: []
    });
};
const createNumberSet = (numberSetParsedKeywords, context) => {
    const numberSubset = createNumberSubset(numberSetParsedKeywords);
    return (0, type_set_1.createTypeSetFromSubsets)('number', [numberSubset], context);
};
exports.createNumberSet = createNumberSet;
