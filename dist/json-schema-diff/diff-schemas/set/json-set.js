"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.createJsonSetFromConfig = exports.emptyJsonSet = exports.allJsonSet = void 0;
const json_set_config_1 = require("./json-set/json-set-config");
const json_set_config_to_core_representation_schema_1 = require("./json-set/json-set-config-to-core-representation-schema");
class AllJsonSet {
    constructor() {
        this.type = 'all';
        this.setType = 'json';
    }
    complement() {
        return exports.emptyJsonSet;
    }
    intersect(other) {
        return other;
    }
    intersectWithSome(other) {
        return other;
    }
    union() {
        return this;
    }
    unionWithSome() {
        return this;
    }
    isSubsetOf(other) {
        return other === this;
    }
    toJsonSchema() {
        return true;
    }
}
class EmptyJsonSet {
    constructor() {
        this.type = 'empty';
        this.setType = 'json';
    }
    complement() {
        return exports.allJsonSet;
    }
    intersect() {
        return this;
    }
    intersectWithSome() {
        return this;
    }
    union(other) {
        return other;
    }
    unionWithSome(other) {
        return other;
    }
    isSubsetOf() {
        return true;
    }
    toJsonSchema() {
        return false;
    }
}
class SomeJsonSet {
    constructor(config) {
        this.config = config;
        this.type = 'some';
        this.setType = 'json';
    }
    complement() {
        return (0, exports.createJsonSetFromConfig)((0, json_set_config_1.complementJsonSetConfig)(this.config));
    }
    intersect(other) {
        return other.intersectWithSome(this);
    }
    intersectWithSome(other) {
        return (0, exports.createJsonSetFromConfig)((0, json_set_config_1.intersectJsonSetConfigs)(this.config, other.config));
    }
    union(other) {
        return other.unionWithSome(this);
    }
    unionWithSome(other) {
        return (0, exports.createJsonSetFromConfig)((0, json_set_config_1.unionJsonSetConfigs)(this.config, other.config));
    }
    isSubsetOf(other) {
        return other.complement().intersect(this).type === 'empty';
    }
    toJsonSchema() {
        return (0, json_set_config_to_core_representation_schema_1.jsonSetConfigToCoreRepresentationSchema)(this.config);
    }
}
const areAllSubsetsInConfigOfType = (config, setType) => {
    return Object
        .keys(config)
        .every((name) => config[name].type === setType);
};
exports.allJsonSet = new AllJsonSet();
exports.emptyJsonSet = new EmptyJsonSet();
const createJsonSetFromConfig = (config) => {
    if (areAllSubsetsInConfigOfType(config, 'empty')) {
        return exports.emptyJsonSet;
    }
    else if (areAllSubsetsInConfigOfType(config, 'all')) {
        return exports.allJsonSet;
    }
    return new SomeJsonSet(config);
};
exports.createJsonSetFromConfig = createJsonSetFromConfig;
