"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.createNumberSubsetFromConfig = exports.emptyNumberSubset = exports.allNumberSubset = void 0;
const complement_number_subset_config_1 = require("./number-subset/complement-number-subset-config");
const intersect_number_subset_config_1 = require("./number-subset/intersect-number-subset-config");
const number_subset_config_has_contradictions_1 = require("./number-subset/number-subset-config-has-contradictions");
const number_subset_config_to_json_schema_1 = require("./number-subset/number-subset-config-to-json-schema");
const simplify_number_subset_config_1 = require("./number-subset/simplify-number-subset-config");
const subset_1 = require("./subset");
class SomeNumberSubset {
    constructor(config) {
        this.config = config;
        this.type = 'some';
        this.setType = 'number';
    }
    complement() {
        return (0, complement_number_subset_config_1.complementNumberSubsetConfig)(this.config).map(exports.createNumberSubsetFromConfig);
    }
    intersect(other) {
        return other.intersectWithSome(this);
    }
    intersectWithSome(other) {
        return (0, exports.createNumberSubsetFromConfig)((0, intersect_number_subset_config_1.intersectNumberSubsetConfig)(this.config, other.config));
    }
    toJsonSchema() {
        return (0, number_subset_config_to_json_schema_1.numberSubsetConfigToJsonSchema)(this.config);
    }
}
exports.allNumberSubset = new subset_1.AllSubset('number');
exports.emptyNumberSubset = new subset_1.EmptySubset('number');
const createNumberSubsetFromConfig = (config) => {
    const simplifiedConfig = (0, simplify_number_subset_config_1.simplifyNumberSubsetConfig)(config);
    return (0, number_subset_config_has_contradictions_1.numberSubsetConfigHasContradictions)(simplifiedConfig)
        ? exports.emptyNumberSubset
        : new SomeNumberSubset(simplifiedConfig);
};
exports.createNumberSubsetFromConfig = createNumberSubsetFromConfig;
