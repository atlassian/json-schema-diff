"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.stringSubsetConfigToJsonSchema = void 0;
const keyword_defaults_1 = require("../../keyword-defaults");
const getMaxLengthSchema = (config) => config.maxLength === keyword_defaults_1.defaultMaxLength ? {} : { maxLength: config.maxLength };
const getMinLengthSchema = (config) => config.minLength === keyword_defaults_1.defaultMinLength ? {} : { minLength: config.minLength };
const stringSubsetConfigToJsonSchema = (config) => (Object.assign(Object.assign({ type: 'string' }, getMaxLengthSchema(config)), getMinLengthSchema(config)));
exports.stringSubsetConfigToJsonSchema = stringSubsetConfigToJsonSchema;
