"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.complementStringSubsetConfig = void 0;
const keyword_defaults_1 = require("../../keyword-defaults");
const complementMaxLength = (config) => ({
    maxLength: keyword_defaults_1.defaultMaxLength,
    minLength: config.maxLength + 1
});
const complementMinLength = (config) => ({
    maxLength: config.minLength - 1,
    minLength: keyword_defaults_1.defaultMinLength
});
const complementStringSubsetConfig = (config) => [
    complementMaxLength(config),
    complementMinLength(config)
];
exports.complementStringSubsetConfig = complementStringSubsetConfig;
