"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.stringSubsetConfigHasContradictions = void 0;
const isMaxLengthAndMinLengthContradiction = (config) => config.minLength > config.maxLength;
const isMinLengthContradiction = (config) => config.minLength === Number.POSITIVE_INFINITY;
const contradictionTests = [
    isMaxLengthAndMinLengthContradiction,
    isMinLengthContradiction
];
const stringSubsetConfigHasContradictions = (config) => contradictionTests.some((contradictionTest) => contradictionTest(config));
exports.stringSubsetConfigHasContradictions = stringSubsetConfigHasContradictions;
