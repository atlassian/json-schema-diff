"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.intersectStringSubsetConfig = void 0;
const intersectMaxLength = (configA, configB) => Math.min(configA.maxLength, configB.maxLength);
const intersectMinLength = (configA, configB) => Math.max(configA.minLength, configB.minLength);
const intersectStringSubsetConfig = (configA, configB) => ({
    maxLength: intersectMaxLength(configA, configB),
    minLength: intersectMinLength(configA, configB)
});
exports.intersectStringSubsetConfig = intersectStringSubsetConfig;
