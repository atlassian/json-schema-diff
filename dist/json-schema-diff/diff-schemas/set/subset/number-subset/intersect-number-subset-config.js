"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.intersectNumberSubsetConfig = void 0;
const intersectNumberSubsetConfig = (configA, configB) => ({
    exclusiveMaximum: Math.min(configA.exclusiveMaximum, configB.exclusiveMaximum),
    exclusiveMinimum: Math.max(configA.exclusiveMinimum, configB.exclusiveMinimum),
    maximum: Math.min(configA.maximum, configB.maximum),
    minimum: Math.max(configA.minimum, configB.minimum),
    isInteger: configA.isInteger || configB.isInteger,
    not: [...configA.not, ...configB.not]
});
exports.intersectNumberSubsetConfig = intersectNumberSubsetConfig;
