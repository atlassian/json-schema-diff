"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.numberSubsetConfigHasContradictions = void 0;
const isIntegerAndNotIsIntegerContradiction = (config) => config.not.some((subset) => subset.isInteger === config.isInteger);
const isMinimumAndMaximumContradiction = (config) => config.minimum >= config.exclusiveMaximum
    || config.minimum > config.maximum
    || config.exclusiveMinimum >= Math.min(config.maximum, config.exclusiveMaximum);
const isIntegerNotInExclusiveRange = (config) => config.isInteger && config.exclusiveMaximum - config.exclusiveMinimum === 1;
const contradictionTests = [
    isIntegerAndNotIsIntegerContradiction,
    isMinimumAndMaximumContradiction,
    isIntegerNotInExclusiveRange
];
const numberSubsetConfigHasContradictions = (config) => contradictionTests.some((contradictionTest) => contradictionTest(config));
exports.numberSubsetConfigHasContradictions = numberSubsetConfigHasContradictions;
