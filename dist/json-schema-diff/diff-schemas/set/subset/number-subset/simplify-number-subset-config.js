"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.simplifyNumberSubsetConfig = void 0;
const keyword_defaults_1 = require("../../keyword-defaults");
const normaliseRangesForIntegers = (config) => config.isInteger
    ? Object.assign(Object.assign({}, config), { minimum: Math.ceil(config.minimum), maximum: Math.floor(config.maximum), exclusiveMinimum: Math.floor(config.exclusiveMinimum), exclusiveMaximum: Math.ceil(config.exclusiveMaximum) }) : Object.assign({}, config);
const mostRestrictiveMinimum = (config) => config.minimum > config.exclusiveMinimum
    ? { minimum: config.minimum, exclusiveMinimum: keyword_defaults_1.defaultMinimum }
    : { minimum: keyword_defaults_1.defaultMinimum, exclusiveMinimum: config.exclusiveMinimum };
const mostRestrictiveMaximum = (config) => config.maximum < config.exclusiveMaximum
    ? { maximum: config.maximum, exclusiveMaximum: keyword_defaults_1.defaultMaximum }
    : { maximum: keyword_defaults_1.defaultMaximum, exclusiveMaximum: config.exclusiveMaximum };
const simplifyNumberSubsetConfig = (config) => {
    config = normaliseRangesForIntegers(config);
    return Object.assign(Object.assign(Object.assign({}, config), mostRestrictiveMinimum(config)), mostRestrictiveMaximum(config));
};
exports.simplifyNumberSubsetConfig = simplifyNumberSubsetConfig;
