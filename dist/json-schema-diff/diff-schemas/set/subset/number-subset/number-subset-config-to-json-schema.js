"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.numberSubsetConfigToJsonSchema = void 0;
const keyword_defaults_1 = require("../../keyword-defaults");
const getNotSchema = (config) => 
// TODO: write test to force an anyOf, is it possible?
config.not.length === 0 ? {} : { not: (0, exports.numberSubsetConfigToJsonSchema)(config.not[0]) };
const getExclusiveMaximum = (config) => config.exclusiveMaximum === keyword_defaults_1.defaultMaximum ? {} : { exclusiveMaximum: config.exclusiveMaximum };
const getExclusiveMinimum = (config) => config.exclusiveMinimum === keyword_defaults_1.defaultMinimum ? {} : { exclusiveMinimum: config.exclusiveMinimum };
const getMaximum = (config) => config.maximum === keyword_defaults_1.defaultMaximum ? {} : { maximum: config.maximum };
const getMinimum = (config) => config.minimum === keyword_defaults_1.defaultMinimum ? {} : { minimum: config.minimum };
const getTypeSchema = (config) => config.isInteger ? { type: 'integer' } : { type: 'number' };
const numberSubsetConfigToJsonSchema = (config) => (Object.assign(Object.assign(Object.assign(Object.assign(Object.assign(Object.assign({}, getTypeSchema(config)), getExclusiveMaximum(config)), getExclusiveMinimum(config)), getMaximum(config)), getMinimum(config)), getNotSchema(config)));
exports.numberSubsetConfigToJsonSchema = numberSubsetConfigToJsonSchema;
