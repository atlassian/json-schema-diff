"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.complementNumberSubsetConfig = void 0;
const keyword_defaults_1 = require("../../keyword-defaults");
const getUnrestrictedNumberSubsetConfig = () => ({
    exclusiveMaximum: keyword_defaults_1.defaultMaximum,
    exclusiveMinimum: keyword_defaults_1.defaultMinimum,
    maximum: keyword_defaults_1.defaultMaximum,
    minimum: keyword_defaults_1.defaultMinimum,
    isInteger: false,
    not: []
});
const complementNot = (config) => config.not;
const complementMaximum = (config) => (Object.assign(Object.assign({}, getUnrestrictedNumberSubsetConfig()), { exclusiveMinimum: config.maximum }));
const complementMinimum = (config) => (Object.assign(Object.assign({}, getUnrestrictedNumberSubsetConfig()), { exclusiveMaximum: config.minimum }));
const complementExclusiveMaximum = (config) => (Object.assign(Object.assign({}, getUnrestrictedNumberSubsetConfig()), { minimum: config.exclusiveMaximum }));
const complementExclusiveMinimum = (config) => (Object.assign(Object.assign({}, getUnrestrictedNumberSubsetConfig()), { maximum: config.exclusiveMinimum }));
const complementIsInteger = (config) => (Object.assign(Object.assign({}, getUnrestrictedNumberSubsetConfig()), { not: [Object.assign(Object.assign({}, getUnrestrictedNumberSubsetConfig()), { isInteger: config.isInteger })] }));
const complementNumberSubsetConfig = (config) => [
    complementExclusiveMaximum(config),
    complementExclusiveMinimum(config),
    complementMaximum(config),
    complementMinimum(config),
    complementIsInteger(config),
    ...complementNot(config)
];
exports.complementNumberSubsetConfig = complementNumberSubsetConfig;
