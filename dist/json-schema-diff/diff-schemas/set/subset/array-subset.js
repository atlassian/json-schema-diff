"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.createArraySubsetFromConfig = exports.emptyArraySubset = exports.allArraySubset = void 0;
const array_subset_config_has_contradictions_1 = require("./array-subset/array-subset-config-has-contradictions");
const array_subset_config_to_json_schema_1 = require("./array-subset/array-subset-config-to-json-schema");
const complement_array_subset_config_1 = require("./array-subset/complement-array-subset-config");
const intersect_array_subset_config_1 = require("./array-subset/intersect-array-subset-config");
const simplify_array_subset_config_1 = require("./array-subset/simplify-array-subset-config");
const subset_1 = require("./subset");
class SomeArraySubset {
    constructor(config) {
        this.config = config;
        this.type = 'some';
        this.setType = 'array';
    }
    complement() {
        return (0, complement_array_subset_config_1.complementArraySubsetConfig)(this.config).map(exports.createArraySubsetFromConfig);
    }
    intersect(other) {
        return other.intersectWithSome(this);
    }
    intersectWithSome(other) {
        return (0, exports.createArraySubsetFromConfig)((0, intersect_array_subset_config_1.intersectArraySubsetConfig)(this.config, other.config));
    }
    toJsonSchema() {
        return (0, array_subset_config_to_json_schema_1.arraySubsetConfigToJsonSchema)(this.config);
    }
}
exports.allArraySubset = new subset_1.AllSubset('array');
exports.emptyArraySubset = new subset_1.EmptySubset('array');
const createArraySubsetFromConfig = (config) => {
    const simplifiedConfig = (0, simplify_array_subset_config_1.simplifyArraySubsetConfig)(config);
    return (0, array_subset_config_has_contradictions_1.arraySubsetConfigHasContradictions)(simplifiedConfig)
        ? exports.emptyArraySubset
        : new SomeArraySubset(simplifiedConfig);
};
exports.createArraySubsetFromConfig = createArraySubsetFromConfig;
