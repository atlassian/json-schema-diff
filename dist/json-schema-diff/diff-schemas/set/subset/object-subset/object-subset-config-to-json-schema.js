"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.objectSubsetConfigToJsonSchema = void 0;
const keyword_defaults_1 = require("../../keyword-defaults");
const set_1 = require("../../set");
const getAdditionalPropertiesSchema = (config) => config.additionalProperties.type === 'all'
    ? {}
    : { additionalProperties: config.additionalProperties.toJsonSchema() };
const getMaxPropertiesSchema = (config) => config.maxProperties === keyword_defaults_1.defaultMaxProperties ? {} : { maxProperties: config.maxProperties };
const getMinPropertiesSchema = (config) => config.minProperties === keyword_defaults_1.defaultMinProperties ? {} : { minProperties: config.minProperties };
const getNotSchema = (config) => config.not.length === 0 ? {} : { not: (0, set_1.addAnyOfIfNecessary)(config.not.map(exports.objectSubsetConfigToJsonSchema)) };
const toSchemaProperties = (properties) => {
    const schemaProperties = {};
    for (const propertyName of Object.keys(properties)) {
        schemaProperties[propertyName] = properties[propertyName].toJsonSchema();
    }
    return schemaProperties;
};
const getPropertiesSchema = (config) => Object.keys(config.properties).length === 0 ? {} : { properties: toSchemaProperties(config.properties) };
const getRequiredSchema = (config) => config.required.length === 0 ? {} : { required: config.required };
const objectSubsetConfigToJsonSchema = (config) => (Object.assign(Object.assign(Object.assign(Object.assign(Object.assign(Object.assign(Object.assign({}, getAdditionalPropertiesSchema(config)), getMaxPropertiesSchema(config)), getMinPropertiesSchema(config)), getNotSchema(config)), getPropertiesSchema(config)), getRequiredSchema(config)), { type: 'object' }));
exports.objectSubsetConfigToJsonSchema = objectSubsetConfigToJsonSchema;
