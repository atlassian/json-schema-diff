"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.unique = void 0;
const _ = require("lodash");
const unique = (arr1, arr2) => _.sortBy(_.uniq(arr1.concat(arr2)));
exports.unique = unique;
