"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.intersectObjectSubsetConfig = void 0;
const object_subset_config_1 = require("./object-subset-config");
const unique_1 = require("./unique");
const intersectMaxProperties = (configA, configB) => Math.min(configA.maxProperties, configB.maxProperties);
const intersectMinProperties = (configA, configB) => Math.max(configA.minProperties, configB.minProperties);
const intersectProperties = (configA, configB) => {
    const allPropertyNames = (0, unique_1.unique)((0, object_subset_config_1.getPropertyNames)(configA), (0, object_subset_config_1.getPropertyNames)(configB));
    const intersectedProperties = {};
    for (const propertyName of allPropertyNames) {
        const propertySetA = (0, object_subset_config_1.getPropertySet)(configA, propertyName);
        const propertySetB = (0, object_subset_config_1.getPropertySet)(configB, propertyName);
        intersectedProperties[propertyName] = propertySetA.intersect(propertySetB);
    }
    return intersectedProperties;
};
const intersectRequired = (configA, configB) => (0, unique_1.unique)(configA.required, configB.required);
const intersectObjectSubsetConfig = (configA, configB) => ({
    additionalProperties: configA.additionalProperties.intersect(configB.additionalProperties),
    maxProperties: intersectMaxProperties(configA, configB),
    minProperties: intersectMinProperties(configA, configB),
    not: [...configA.not, ...configB.not],
    properties: intersectProperties(configA, configB),
    required: intersectRequired(configA, configB)
});
exports.intersectObjectSubsetConfig = intersectObjectSubsetConfig;
