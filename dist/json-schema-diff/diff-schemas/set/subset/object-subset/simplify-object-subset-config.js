"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.simplifyObjectSubsetConfig = void 0;
const json_set_1 = require("../../json-set");
const keyword_defaults_1 = require("../../keyword-defaults");
const maxPropertiesAllowsNoProperties = (config) => config.maxProperties === 0;
/*
All ObjectSubsetConfig objects go through this simplification when being created. The only way 2 or more entries can
appear inside the not array is when two existing ObjectSubsetConfigs are intersected, both of which are simplified
already. Therefore we only need to apply this simplification when config.not === 1.
*/
const notDisallowsObjectsWithNoProperties = (config) => config.not.length === 1
    && config.not[0].additionalProperties.type === 'empty'
    && Object.keys(config.not[0].properties).length === 0;
const simplifyObjectSubsetConfig = (config) => {
    if (maxPropertiesAllowsNoProperties(config)) {
        return Object.assign(Object.assign({}, config), { maxProperties: keyword_defaults_1.defaultMaxProperties, additionalProperties: json_set_1.emptyJsonSet });
    }
    if (notDisallowsObjectsWithNoProperties(config)) {
        return Object.assign(Object.assign({}, config), { not: [], minProperties: 1 });
    }
    return config;
};
exports.simplifyObjectSubsetConfig = simplifyObjectSubsetConfig;
