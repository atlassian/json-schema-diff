"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.objectSubsetConfigHasContradictions = void 0;
const object_subset_config_1 = require("./object-subset-config");
const isMinPropertiesBiggerThanDefinedProperties = (config) => {
    const numberOfDefinedPropertiesInSchema = Object.values(config.properties)
        .filter((propertySchema) => propertySchema.type !== 'empty')
        .length;
    return config.minProperties > numberOfDefinedPropertiesInSchema;
};
const areAdditionalPropertiesNotAllowed = (config) => config.additionalProperties.type === 'empty';
const isMinPropertiesAndAdditionalPropertiesContradiction = (config) => areAdditionalPropertiesNotAllowed(config) && isMinPropertiesBiggerThanDefinedProperties(config);
const isRequiredAndPropertiesAndAdditionalPropertiesContradiction = (config) => config.required.some((propertyName) => (0, object_subset_config_1.getPropertySet)(config, propertyName).type === 'empty');
const isMinPropertiesAndMaxPropertiesContradiction = (config) => config.minProperties > config.maxProperties;
const isMinPropertiesContradiction = (config) => config.minProperties === Number.POSITIVE_INFINITY;
const isMaxPropertiesAndRequiredContradiction = (config) => config.required.length > config.maxProperties;
const isAdditionalPropertiesAndNotAdditionalPropertiesContradiction = (config) => 
// TODO: When we support 'not', does this need to look at not.properties?
config.not.some((notConfig) => config.additionalProperties.isSubsetOf(notConfig.additionalProperties));
const contradictionTests = [
    isRequiredAndPropertiesAndAdditionalPropertiesContradiction,
    isMinPropertiesAndAdditionalPropertiesContradiction,
    isMinPropertiesAndMaxPropertiesContradiction,
    isMinPropertiesContradiction,
    isMaxPropertiesAndRequiredContradiction,
    isAdditionalPropertiesAndNotAdditionalPropertiesContradiction
];
const objectSubsetConfigHasContradictions = (config) => contradictionTests.some((contradictionTest) => contradictionTest(config));
exports.objectSubsetConfigHasContradictions = objectSubsetConfigHasContradictions;
