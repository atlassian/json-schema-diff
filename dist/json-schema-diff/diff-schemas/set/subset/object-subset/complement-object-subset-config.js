"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.complementObjectSubsetConfig = void 0;
const json_set_1 = require("../../json-set");
const keyword_defaults_1 = require("../../keyword-defaults");
const object_subset_config_1 = require("./object-subset-config");
const complementAdditionalProperties = (config) => ({
    additionalProperties: json_set_1.allJsonSet,
    maxProperties: keyword_defaults_1.defaultMaxProperties,
    minProperties: keyword_defaults_1.defaultMinProperties,
    not: [{
            additionalProperties: config.additionalProperties,
            maxProperties: keyword_defaults_1.defaultMaxProperties,
            minProperties: keyword_defaults_1.defaultMinProperties,
            not: [],
            properties: config.properties,
            required: keyword_defaults_1.defaultRequired
        }],
    properties: keyword_defaults_1.defaultProperties,
    required: keyword_defaults_1.defaultRequired
});
const complementMaxProperties = (config) => ({
    additionalProperties: json_set_1.allJsonSet,
    maxProperties: keyword_defaults_1.defaultMaxProperties,
    minProperties: config.maxProperties + 1,
    not: [],
    properties: keyword_defaults_1.defaultProperties,
    required: keyword_defaults_1.defaultRequired
});
const complementMinProperties = (config) => ({
    additionalProperties: json_set_1.allJsonSet,
    maxProperties: config.minProperties - 1,
    minProperties: keyword_defaults_1.defaultMinProperties,
    not: [],
    properties: keyword_defaults_1.defaultProperties,
    required: keyword_defaults_1.defaultRequired
});
const complementNot = (config) => config.not;
const complementProperties = (config) => (0, object_subset_config_1.getPropertyNames)(config).map((propertyName) => {
    const complementedPropertySet = (0, object_subset_config_1.getPropertySet)(config, propertyName).complement();
    return {
        additionalProperties: json_set_1.allJsonSet,
        maxProperties: keyword_defaults_1.defaultMaxProperties,
        minProperties: keyword_defaults_1.defaultMinProperties,
        not: [],
        properties: { [propertyName]: complementedPropertySet },
        required: [propertyName]
    };
});
const complementRequiredProperties = (config) => config.required.map((requiredPropertyName) => ({
    additionalProperties: json_set_1.allJsonSet,
    maxProperties: keyword_defaults_1.defaultMaxProperties,
    minProperties: keyword_defaults_1.defaultMinProperties,
    not: [],
    properties: {
        [requiredPropertyName]: json_set_1.emptyJsonSet
    },
    required: keyword_defaults_1.defaultRequired
}));
const complementObjectSubsetConfig = (config) => [
    complementAdditionalProperties(config),
    complementMaxProperties(config),
    complementMinProperties(config),
    ...complementNot(config),
    ...complementProperties(config),
    ...complementRequiredProperties(config)
];
exports.complementObjectSubsetConfig = complementObjectSubsetConfig;
