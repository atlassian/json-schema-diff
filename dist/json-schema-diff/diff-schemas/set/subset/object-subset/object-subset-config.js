"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getPropertySet = exports.getPropertyNames = void 0;
const getPropertyNames = (config) => Object.keys(config.properties);
exports.getPropertyNames = getPropertyNames;
const getPropertySet = (config, propertyName) => Object.getOwnPropertyNames(config.properties).includes(propertyName)
    ? config.properties[propertyName]
    : config.additionalProperties;
exports.getPropertySet = getPropertySet;
