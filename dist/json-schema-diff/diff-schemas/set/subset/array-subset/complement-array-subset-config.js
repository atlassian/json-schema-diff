"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.complementArraySubsetConfig = void 0;
const json_set_1 = require("../../json-set");
const keyword_defaults_1 = require("../../keyword-defaults");
const complementItems = (config) => ({
    items: json_set_1.allJsonSet,
    maxItems: keyword_defaults_1.defaultMaxItems,
    minItems: keyword_defaults_1.defaultMinItems,
    not: [{
            items: config.items,
            maxItems: keyword_defaults_1.defaultMaxItems,
            minItems: keyword_defaults_1.defaultMinItems,
            not: []
        }]
});
const complementMaxItems = (config) => ({
    items: json_set_1.allJsonSet,
    maxItems: keyword_defaults_1.defaultMaxItems,
    minItems: config.maxItems + 1,
    not: []
});
const complementMinItems = (config) => ({
    items: json_set_1.allJsonSet,
    maxItems: config.minItems - 1,
    minItems: keyword_defaults_1.defaultMinItems,
    not: []
});
const complementNot = (config) => config.not;
const complementArraySubsetConfig = (config) => [
    complementItems(config),
    complementMaxItems(config),
    complementMinItems(config),
    ...complementNot(config)
];
exports.complementArraySubsetConfig = complementArraySubsetConfig;
