"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.intersectArraySubsetConfig = void 0;
const intersectMaxItems = (configA, configB) => Math.min(configA.maxItems, configB.maxItems);
const intersectMinItems = (configA, configB) => Math.max(configA.minItems, configB.minItems);
const intersectArraySubsetConfig = (configA, configB) => ({
    items: configA.items.intersect(configB.items),
    maxItems: intersectMaxItems(configA, configB),
    minItems: intersectMinItems(configA, configB),
    not: [...configA.not, ...configB.not]
});
exports.intersectArraySubsetConfig = intersectArraySubsetConfig;
