"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.simplifyArraySubsetConfig = void 0;
const json_set_1 = require("../../json-set");
const keyword_defaults_1 = require("../../keyword-defaults");
const maxItemsAllowsNoItems = (config) => config.maxItems === 0;
/*
All ArraySubsetConfig objects go through this simplification when being created. The only way 2 or more entries can
appear inside the not array is when two existing ArraySubsetConfig are intersected, both of which are simplified
already. Therefore we only need to apply this simplification when config.not === 1.
*/
const notItemsDisallowsEmptyArrays = (config) => config.not.length === 1 && config.not[0].items.type === 'empty';
const simplifyArraySubsetConfig = (config) => {
    if (maxItemsAllowsNoItems(config)) {
        return Object.assign(Object.assign({}, config), { items: json_set_1.emptyJsonSet, maxItems: keyword_defaults_1.defaultMaxItems });
    }
    if (notItemsDisallowsEmptyArrays(config)) {
        return Object.assign(Object.assign({}, config), { not: [], minItems: 1 });
    }
    return config;
};
exports.simplifyArraySubsetConfig = simplifyArraySubsetConfig;
