"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.arraySubsetConfigHasContradictions = void 0;
const isEmptyItemsAndMinItemsContradiction = (config) => {
    const itemsAcceptsNoValues = config.items.type === 'empty';
    const minItemsRequiresValues = config.minItems > 0;
    return itemsAcceptsNoValues && minItemsRequiresValues;
};
const isMaxItemsAndMinItemsContradiction = (config) => config.minItems > config.maxItems;
const isMinItemsContradiction = (config) => config.minItems === Number.POSITIVE_INFINITY;
const isItemsAndNotItemsContradiction = (config) => config.not.some((notConfig) => config.items.isSubsetOf(notConfig.items));
const contradictionTests = [
    isEmptyItemsAndMinItemsContradiction,
    isMaxItemsAndMinItemsContradiction,
    isMinItemsContradiction,
    isItemsAndNotItemsContradiction
];
const arraySubsetConfigHasContradictions = (config) => contradictionTests.some((contradictionTest) => contradictionTest(config));
exports.arraySubsetConfigHasContradictions = arraySubsetConfigHasContradictions;
