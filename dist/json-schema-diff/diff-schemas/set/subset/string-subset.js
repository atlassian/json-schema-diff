"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.createStringSubsetFromConfig = exports.emptyStringSubset = exports.allStringSubset = void 0;
const complement_string_subset_config_1 = require("./string-subset/complement-string-subset-config");
const intersect_string_subset_config_1 = require("./string-subset/intersect-string-subset-config");
const string_subset_config_has_contradictions_1 = require("./string-subset/string-subset-config-has-contradictions");
const string_subset_config_to_json_schema_1 = require("./string-subset/string-subset-config-to-json-schema");
const subset_1 = require("./subset");
class SomeStringSubset {
    constructor(config) {
        this.config = config;
        this.type = 'some';
        this.setType = 'string';
    }
    complement() {
        return (0, complement_string_subset_config_1.complementStringSubsetConfig)(this.config).map(exports.createStringSubsetFromConfig);
    }
    intersect(other) {
        return other.intersectWithSome(this);
    }
    intersectWithSome(other) {
        return (0, exports.createStringSubsetFromConfig)((0, intersect_string_subset_config_1.intersectStringSubsetConfig)(this.config, other.config));
    }
    toJsonSchema() {
        return (0, string_subset_config_to_json_schema_1.stringSubsetConfigToJsonSchema)(this.config);
    }
}
exports.allStringSubset = new subset_1.AllSubset('string');
exports.emptyStringSubset = new subset_1.EmptySubset('string');
const createStringSubsetFromConfig = (config) => (0, string_subset_config_has_contradictions_1.stringSubsetConfigHasContradictions)(config)
    ? exports.emptyStringSubset
    : new SomeStringSubset(config);
exports.createStringSubsetFromConfig = createStringSubsetFromConfig;
