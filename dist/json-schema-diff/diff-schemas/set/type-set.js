"use strict";
/* eslint-disable max-classes-per-file */
Object.defineProperty(exports, "__esModule", { value: true });
exports.createTypeSetFromSubsets = void 0;
const set_1 = require("./set");
class AllTypeSet {
    constructor(setType) {
        this.setType = setType;
        this.type = 'all';
    }
    intersect(other) {
        return other;
    }
    intersectWithSome(other) {
        return other;
    }
    union() {
        return this;
    }
    unionWithSome() {
        return this;
    }
    complement() {
        return new EmptyTypeSet(this.setType);
    }
    isSubsetOf() {
        throw new Error('Not Implemented');
    }
    toJsonSchema() {
        return { type: this.setType };
    }
}
class EmptyTypeSet {
    constructor(setType) {
        this.setType = setType;
        this.type = 'empty';
    }
    intersect() {
        return this;
    }
    intersectWithSome() {
        return this;
    }
    union(other) {
        return other;
    }
    unionWithSome(other) {
        return other;
    }
    complement() {
        return new AllTypeSet(this.setType);
    }
    isSubsetOf() {
        return true;
    }
    toJsonSchema() {
        return false;
    }
}
class SomeTypeSet {
    constructor(setType, subsets, context) {
        this.setType = setType;
        this.subsets = subsets;
        this.context = context;
        this.type = 'some';
    }
    complement() {
        const complementedSubsets = this.subsets
            .map((set) => set.complement())
            .reduce((subsetListA, subsetListB) => this.intersectSubsets(subsetListA, subsetListB));
        return (0, exports.createTypeSetFromSubsets)(this.setType, complementedSubsets, this.context);
    }
    intersect(other) {
        return other.intersectWithSome(this);
    }
    intersectWithSome(other) {
        const intersectedSubsets = this.intersectSubsets(this.subsets, other.subsets);
        return (0, exports.createTypeSetFromSubsets)(this.setType, intersectedSubsets, this.context);
    }
    union(other) {
        return other.unionWithSome(this);
    }
    unionWithSome(other) {
        const newSubsets = [...this.subsets, ...other.subsets];
        return (0, exports.createTypeSetFromSubsets)(this.setType, newSubsets, this.context);
    }
    isSubsetOf(other) {
        return other.complement().intersect(this).type === 'empty';
    }
    toJsonSchema() {
        return (0, set_1.addAnyOfIfNecessary)(this.subsets.map((subset) => subset.toJsonSchema()));
    }
    intersectSubsets(subsetListA, subsetListB) {
        let intersectedSubsets = [];
        for (const subsetA of subsetListA) {
            for (const subsetB of subsetListB) {
                const intersectionSubset = subsetA.intersect(subsetB);
                const intersectionTypeSet = (0, exports.createTypeSetFromSubsets)(intersectionSubset.setType, [intersectionSubset], this.context);
                if (this.shouldOmitIntersectionBasedOnHeuristics(intersectedSubsets, intersectionTypeSet)) {
                    continue;
                }
                intersectedSubsets = this.simplifyIntersectedSubsetsWithHeuristics(intersectedSubsets, intersectionTypeSet);
                intersectedSubsets.push({ subset: intersectionSubset, typeSet: intersectionTypeSet });
            }
        }
        return intersectedSubsets.map((s) => s.subset);
    }
    shouldOmitIntersectionBasedOnHeuristics(intersectedSubsets, intersectionTypeSet) {
        if (this.context.heuristics.applyLawOfAbsorptionInTypeSetCartesianProduct) {
            const isIntersectionAlreadyRepresented = intersectedSubsets.some((s) => intersectionTypeSet.isSubsetOf(s.typeSet));
            return isIntersectionAlreadyRepresented;
        }
        return false;
    }
    simplifyIntersectedSubsetsWithHeuristics(intersectedSubsets, intersectionTypeSet) {
        return this.context.heuristics.applyLawOfAbsorptionInTypeSetCartesianProduct
            ? intersectedSubsets.filter((s) => !s.typeSet.isSubsetOf(intersectionTypeSet))
            : intersectedSubsets;
    }
}
const isAnySubsetTypeAll = (subsets) => subsets.some((subset) => subset.type === 'all');
const isTypeAll = (subsets) => {
    return isAnySubsetTypeAll(subsets);
};
const getNonEmptySubsets = (subsets) => subsets.filter((subset) => subset.type !== 'empty');
const createTypeSetFromSubsets = (setType, subsets, context) => {
    const notEmptySubsets = getNonEmptySubsets(subsets);
    if (notEmptySubsets.length === 0) {
        return new EmptyTypeSet(setType);
    }
    if (isTypeAll(subsets)) {
        return new AllTypeSet(setType);
    }
    return new SomeTypeSet(setType, notEmptySubsets, context);
};
exports.createTypeSetFromSubsets = createTypeSetFromSubsets;
