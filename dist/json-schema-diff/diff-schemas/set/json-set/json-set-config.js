"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.complementJsonSetConfig = exports.unionJsonSetConfigs = exports.intersectJsonSetConfigs = void 0;
const mapJsonSetConfigs = (a, b, callbackFn) => ({
    array: callbackFn(a.array, b.array),
    boolean: callbackFn(a.boolean, b.boolean),
    null: callbackFn(a.null, b.null),
    number: callbackFn(a.number, b.number),
    object: callbackFn(a.object, b.object),
    string: callbackFn(a.string, b.string)
});
const intersectJsonSetConfigs = (configA, configB) => mapJsonSetConfigs(configA, configB, (typeSet1, typeSet2) => typeSet1.intersect(typeSet2));
exports.intersectJsonSetConfigs = intersectJsonSetConfigs;
const unionJsonSetConfigs = (configA, configB) => mapJsonSetConfigs(configA, configB, (typeSet1, typeSet2) => typeSet1.union(typeSet2));
exports.unionJsonSetConfigs = unionJsonSetConfigs;
const complementJsonSetConfig = (config) => ({
    array: config.array.complement(),
    boolean: config.boolean.complement(),
    null: config.null.complement(),
    number: config.number.complement(),
    object: config.object.complement(),
    string: config.string.complement()
});
exports.complementJsonSetConfig = complementJsonSetConfig;
