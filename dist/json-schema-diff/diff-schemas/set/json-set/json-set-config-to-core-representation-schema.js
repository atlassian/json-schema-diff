"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.jsonSetConfigToCoreRepresentationSchema = void 0;
const set_1 = require("../set");
const createEmptyCoreSchemaMetaSchema = () => ({ type: [] });
const getTypeOrEmpty = (jsonSchema) => {
    if (!jsonSchema.type) {
        return [];
    }
    if (typeof jsonSchema.type === 'string') {
        return [jsonSchema.type];
    }
    return jsonSchema.type;
};
const getAnyOfOrEmpty = (jsonSchema) => jsonSchema.anyOf || [];
const isSimpleSchema = (schema) => schema.anyOf === undefined;
const mergeTypes = (schema, otherSchema) => {
    const schemaTypes = getTypeOrEmpty(schema);
    const otherSchemaTypes = getTypeOrEmpty(otherSchema);
    const mergedTypes = schemaTypes.concat(otherSchemaTypes);
    return (mergedTypes.length === 1) ? mergedTypes[0] : mergedTypes;
};
const mergeCoreRepresentationJsonSchemas = (schema, otherSchema) => {
    const mergedSchema = Object.assign(Object.assign(Object.assign({}, schema), otherSchema), { type: mergeTypes(schema, otherSchema) });
    if (schema.not && otherSchema.not) {
        mergedSchema.not = mergeCoreRepresentationJsonSchemas(toCoreRepresentationJsonSchema(schema.not), toCoreRepresentationJsonSchema(otherSchema.not));
    }
    return mergedSchema;
};
const isCoreSchemaMetaSchema = (schema) => typeof schema !== 'boolean';
const toCoreRepresentationJsonSchema = (schema) => isCoreSchemaMetaSchema(schema)
    ? schema
    : createEmptyCoreSchemaMetaSchema();
const jsonSetConfigToCoreRepresentationSchema = (config) => {
    const typeSetSchemas = Object
        .keys(config)
        .map((typeSetName) => config[typeSetName].toJsonSchema())
        .filter(isCoreSchemaMetaSchema);
    const schemas = typeSetSchemas
        .filter((schema) => !isSimpleSchema(schema))
        .reduce((accumulator, schema) => {
        const anyOf = getAnyOfOrEmpty(schema);
        return accumulator.concat(anyOf);
    }, []);
    const mergedSimpleSubsetSchemas = typeSetSchemas
        .filter(isSimpleSchema)
        .reduce((mergedSchema, schema) => {
        return mergeCoreRepresentationJsonSchemas(mergedSchema, schema);
    }, createEmptyCoreSchemaMetaSchema());
    if (getTypeOrEmpty(mergedSimpleSubsetSchemas).length > 0) {
        schemas.push(mergedSimpleSubsetSchemas);
    }
    return (0, set_1.addAnyOfIfNecessary)(schemas);
};
exports.jsonSetConfigToCoreRepresentationSchema = jsonSetConfigToCoreRepresentationSchema;
