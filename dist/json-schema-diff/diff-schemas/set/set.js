"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.addAnyOfIfNecessary = void 0;
const addAnyOfIfNecessary = (schemas) => schemas.length === 1 ? schemas[0] : { anyOf: schemas };
exports.addAnyOfIfNecessary = addAnyOfIfNecessary;
