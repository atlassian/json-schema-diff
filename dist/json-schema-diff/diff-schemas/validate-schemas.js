"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.validateSchemas = void 0;
const ajv_1 = require("ajv");
// eslint-disable-next-line @typescript-eslint/no-var-requires, import/no-internal-modules
const jsonSchemaDraft7Schema = require('json-schema-spec-types/lib/json-schema-draft-07-schema');
const validateJsonSchema = (schema) => {
    const ajv = new ajv_1.default({ strict: false, meta: false });
    ajv.addMetaSchema(jsonSchemaDraft7Schema);
    const valid = ajv.validateSchema(schema);
    if (valid) {
        return { isValid: true };
    }
    return {
        failureReason: ajv.errorsText(ajv.errors || undefined),
        isValid: false
    };
};
const validateSchemas = (sourceSchema, destinationSchema) => {
    const sourceSchemaValidationResult = validateJsonSchema(sourceSchema);
    if (!sourceSchemaValidationResult.isValid) {
        return Promise.reject(new Error(`Source schema is not a valid json schema: ${sourceSchemaValidationResult.failureReason}`));
    }
    const destinationSchemaValidationResult = validateJsonSchema(destinationSchema);
    if (!destinationSchemaValidationResult.isValid) {
        return Promise.reject(new Error(`Destination schema is not a valid json schema: ${destinationSchemaValidationResult.failureReason}`));
    }
    return Promise.resolve();
};
exports.validateSchemas = validateSchemas;
