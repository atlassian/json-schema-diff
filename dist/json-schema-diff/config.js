"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.loadConfig = void 0;
const convict = require("convict");
const createConvictInstance = (env) => convict({
    parseContext: {
        heuristics: {
            applyLawOfAbsorptionInTypeSetCartesianProduct: {
                default: true,
                env: 'JSON_SCHEMA_DIFF_APPLY_ABSORPTION_IN_CARTESIAN_PRODUCT',
                format: Boolean
            }
        }
    }
}, { env });
const loadConfig = (env) => {
    const config = createConvictInstance(env);
    config.validate({ allowed: 'strict' });
    return config.getProperties();
};
exports.loadConfig = loadConfig;
